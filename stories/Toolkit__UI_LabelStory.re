open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Label.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Form/Label", ());

let sample = () =>
  <div>
    <Label htmlFor="test"> "Label"->React.string </Label>
    <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
      "Label"->React.string
    </Label>
  </div>;

sample->CSF.addMeta(
  ~name="sample",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<div>
  <Label htmlFor="test"> "Label"->React.string </Label>
  <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
    "Label"->React.string
  </Label>
</div>
          |j}
          />
          <Docs.Story id="form-label--sample" />
        </>,
    },
  },
  (),
);
