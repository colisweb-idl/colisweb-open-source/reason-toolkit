open BsStorybook;
open Storybook;

let default = CSF.make(~title="Components/IconButton", ());

let defaultPalette = () => {
  let colors =
    Js.Dict.fromArray([|
      ("primary", `primary),
      ("black", `black),
      ("white", `white),
      ("gray", `gray),
      ("success", `success),
      ("info", `info),
      ("danger", `danger),
      ("warning", `warning),
      ("neutral", `neutral),
      ("neutralLight", `neutralLight),
    |]);

  let sizes =
    Js.Dict.fromArray([|
      ("xs", `xs),
      ("sm", `sm),
      ("md", `md),
      ("lg", `lg),
    |]);

  <div>
    {colors
     ->Js.Dict.entries
     ->Array.mapWithIndex((i, (colorText, color)) => {
         <div
           key={i->Int.toString ++ colorText}
           className="flex flex-row flex-col-gap-4 mb-4 items-center">
           <strong className="w-40">
             colorText->React.string
           </strong>
           <div className="flex flex-col">
             {[|false, true|]
              ->Array.mapWithIndex((j, disabled) => {
                  <div
                    className="flex items-center flex-col-gap-1 mb-2">
                    {sizes
                     ->Js.Dict.entries
                     ->Array.mapWithIndex((k, (sizeText, size)) => {
                         <div
                           key={
                             (i + j + k)->Int.toString ++ colorText ++ sizeText
                           }>
                           <Toolkit__Ui_IconButton
                             color
                             size
                             disabled
                             icon={<BsReactIcons.MdHome />}
                           />
                         </div>
                       })
                     ->React.array}
                  </div>
                })
              ->React.array}
           </div>
         </div>
       })
     ->React.array}
  </div>;
};

defaultPalette->CSF.addMeta(
  ~name="default palette",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <Docs.Title />
          <Docs.Source
            language="reason"
            code={j|
             <IconButton
  color=`primary
  icon={<BsReactIcons.MdHome />}
/>
          |j}
          />
          <Toolkit__Ui_IconButton
            color=`primary
            icon={<BsReactIcons.MdHome />}
          />
        </>,
    },
  },
  (),
);

let outlinePalette = () => {
  let colors =
    Js.Dict.fromArray([|
      ("primary", `primary),
      ("black", `black),
      ("white", `white),
      ("gray", `gray),
      ("success", `success),
      ("info", `info),
      ("danger", `danger),
      ("warning", `warning),
      ("neutral", `neutral),
      ("neutralLight", `neutralLight),
    |]);

  let sizes =
    Js.Dict.fromArray([|
      ("xs", `xs),
      ("sm", `sm),
      ("md", `md),
      ("lg", `lg),
    |]);

  <div>
    {colors
     ->Js.Dict.entries
     ->Array.mapWithIndex((i, (colorText, color)) => {
         <div
           key={i->Int.toString ++ colorText}
           className="flex flex-row flex-col-gap-4 mb-4 items-center">
           <strong className="w-40">
             colorText->React.string
           </strong>
           <div className="flex flex-col">
             {[|false, true|]
              ->Array.mapWithIndex((j, disabled) => {
                  <div
                    className="flex items-center flex-col-gap-1 mb-2">
                    {sizes
                     ->Js.Dict.entries
                     ->Array.mapWithIndex((k, (sizeText, size)) => {
                         <div
                           key={
                             (i + j + k)->Int.toString ++ colorText ++ sizeText
                           }>
                           <Toolkit__Ui_IconButton
                             color
                             size
                             disabled
                             icon={<BsReactIcons.MdHome />}
                           />
                         </div>
                       })
                     ->React.array}
                  </div>
                })
              ->React.array}
           </div>
         </div>
       })
     ->React.array}
  </div>;
};

outlinePalette->CSF.addMeta(
  ~name="outline palette",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <Docs.Title />
          <Docs.Source
            language="reason"
            code={j|
            <IconButton
  color=`primary
  variant=`outline
  icon={<BsReactIcons.MdHome />}
/>
          |j}
          />
          <Toolkit__Ui_IconButton
            color=`primary
            variant=`outline
            icon={<BsReactIcons.MdHome />}
          />
        </>,
    },
  },
  (),
);
