import tailwindConfig from "../src/tailwind/tailwind.config";

const colors = tailwindConfig.theme.colors;

const ColorPreview = ({ color, code }) => {
  return (
    <div
      className="text-center hover:opacity-70 cursor-pointer"
      onClick={() => {
        navigator.clipboard.writeText(color);
      }}
    >
      <div
        className="p-1 border rounded"
        style={{
          width: 80,
          height: 80,
        }}
      >
        <div
          className="w-full h-full rounded"
          style={{
            backgroundColor: color,
          }}
        />
      </div>
      <p className="text-xs">{color}</p>
      <p>{code}</p>
    </div>
  );
};

export default () => (
  <div>
    {Object.entries(colors).map(([colorType, colors]) => {
      return (
        <div className="mb-6" key={colorType}>
          <h3 className="text-xl font-bold mb-2">{colorType}</h3>
          <div className="flex flex-col-gap-4">
            {typeof colors === "string" ? (
              <ColorPreview code={null} color={colors} />
            ) : (
              Object.entries(colors).map((color) => {
                return (
                  <ColorPreview
                    key={`${colorType}-${color[0]}`}
                    code={color[0]}
                    color={color[1]}
                  />
                );
              })
            )}
          </div>
        </div>
      );
    })}
  </div>
);
