open BsStorybook;
open Storybook;

let default = CSF.make(~title="Components/Button", ());

let defaultPalette = () => {
  let colors =
    Js.Dict.fromArray([|
      ("primary", `primary),
      ("black", `black),
      ("white", `white),
      ("gray", `gray),
      ("success", `success),
      ("info", `info),
      ("danger", `danger),
      ("warning", `warning),
      ("neutral", `neutral),
      ("neutralLight", `neutralLight),
    |]);

  let sizes =
    Js.Dict.fromArray([|
      ("xs", `xs),
      ("sm", `sm),
      ("md", `md),
      ("lg", `lg),
    |]);

  <div>
    {colors
     ->Js.Dict.entries
     ->Array.mapWithIndex((i, (colorText, color)) => {
         <div
           key={i->Int.toString ++ colorText}
           className="flex flex-row flex-col-gap-4 mb-4 items-center">
           <strong className="w-40"> colorText->React.string </strong>
           <div className="flex flex-col">
             {[|false, true|]
              ->Array.mapWithIndex((j, disabled) => {
                  <div
                    className="flex items-center flex-col-gap-1 mb-2">
                    {sizes
                     ->Js.Dict.entries
                     ->Array.mapWithIndex((k, (sizeText, size)) => {
                         <div
                           key={
                             (i + j + k)->Int.toString ++ colorText ++ sizeText
                           }>
                           <Toolkit__Ui_Button color size disabled>
                             "hello"->React.string
                           </Toolkit__Ui_Button>
                         </div>
                       })
                     ->React.array}
                  </div>
                })
              ->React.array}
           </div>
         </div>
       })
     ->React.array}
  </div>;
};

defaultPalette->CSF.addMeta(
  ~name="default palette",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <Docs.Title />
          <Docs.Source
            language="reason"
            code={j|
            <Button variant=`default>"Content"->React.string</Button>
          |j}
          />
        </>,
    },
  },
  (),
);

let pillPalette = () => {
  let colors =
    Js.Dict.fromArray([|
      ("primary", `primary),
      ("black", `black),
      ("white", `white),
      ("gray", `gray),
      ("success", `success),
      ("info", `info),
      ("danger", `danger),
      ("warning", `warning),
      ("neutral", `neutral),
      ("neutralLight", `neutralLight),
    |]);

  let sizes =
    Js.Dict.fromArray([|
      ("xs", `xs),
      ("sm", `sm),
      ("md", `md),
      ("lg", `lg),
    |]);

  <div>
    {colors
     ->Js.Dict.entries
     ->Array.mapWithIndex((i, (colorText, color)) => {
         <div
           key={i->Int.toString ++ colorText}
           className="flex flex-row flex-col-gap-4 mb-4 items-center">
           <strong className="w-40">
             colorText->React.string
           </strong>
           <div className="flex flex-col">
             {[|false, true|]
              ->Array.mapWithIndex((j, disabled) => {
                  <div
                    className="flex items-center flex-col-gap-1 mb-2">
                    {sizes
                     ->Js.Dict.entries
                     ->Array.mapWithIndex((k, (sizeText, size)) => {
                         <div
                           key={
                             (i + j + k)->Int.toString ++ colorText ++ sizeText
                           }>
                           <Toolkit__Ui_Button
                             variant=`pill color size disabled>
                             "hello"->React.string
                           </Toolkit__Ui_Button>
                         </div>
                       })
                     ->React.array}
                  </div>
                })
              ->React.array}
           </div>
         </div>
       })
     ->React.array}
  </div>;
};

pillPalette->CSF.addMeta(
  ~name="pill palette",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <Docs.Title />
          <Docs.Source
            language="reason"
            code={j|
            <Button variant=`pill>"Content"->React.string</Button>
          |j}
          />
        </>,
    },
  },
  (),
);

let outlinePalette = () => {
  let colors =
    Js.Dict.fromArray([|
      ("primary", `primary),
      ("black", `black),
      ("white", `white),
      ("gray", `gray),
      ("success", `success),
      ("info", `info),
      ("danger", `danger),
      ("warning", `warning),
      ("neutral", `neutral),
      ("neutralLight", `neutralLight),
    |]);

  let sizes =
    Js.Dict.fromArray([|
      ("xs", `xs),
      ("sm", `sm),
      ("md", `md),
      ("lg", `lg),
    |]);
  <div>
    {colors
     ->Js.Dict.entries
     ->Array.mapWithIndex((i, (colorText, color)) => {
         <div
           key={i->Int.toString ++ colorText}
           className="flex flex-row flex-col-gap-4 mb-4 items-center">
           <strong className="w-40">
             colorText->React.string
           </strong>
           <div className="flex flex-col">
             {[|false, true|]
              ->Array.mapWithIndex((j, disabled) => {
                  <div
                    className="flex items-center flex-col-gap-1 mb-2">
                    {sizes
                     ->Js.Dict.entries
                     ->Array.mapWithIndex((k, (sizeText, size)) => {
                         <div
                           key={
                             (i + j + k)->Int.toString ++ colorText ++ sizeText
                           }>
                           <Toolkit__Ui_Button
                             color size disabled variant=`outline>
                             "hello"->React.string
                           </Toolkit__Ui_Button>
                         </div>
                       })
                     ->React.array}
                  </div>
                })
              ->React.array}
           </div>
         </div>
       })
     ->React.array}
  </div>;
};

outlinePalette->CSF.addMeta(
  ~name="outline palette",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <Docs.Title />
          <Docs.Source
            language="reason"
            code={j|
            <Button color=`primary variant=`outline>"Content"->React.string</Button>
          |j}
          />
          <Toolkit__Ui_Button color=`primary variant=`outline>
            "hello"->React.string
          </Toolkit__Ui_Button>
        </>,
    },
  },
  (),
);

let textPalette = () => {
  let colors =
    Js.Dict.fromArray([|
      ("primary", `primary),
      ("black", `black),
      ("white", `white),
      ("gray", `gray),
      ("success", `success),
      ("info", `info),
      ("danger", `danger),
      ("warning", `warning),
      ("neutral", `neutral),
      ("neutralLight", `neutralLight),
    |]);

  let sizes =
    Js.Dict.fromArray([|
      ("xs", `xs),
      ("sm", `sm),
      ("md", `md),
      ("lg", `lg),
    |]);
  <div>
    {colors
     ->Js.Dict.entries
     ->Array.mapWithIndex((i, (colorText, color)) => {
         <div
           key={i->Int.toString ++ colorText}
           className="flex flex-row flex-col-gap-4 mb-4 items-center">
           <strong className="w-40">
             colorText->React.string
           </strong>
           <div className="flex flex-col">
             {[|false, true|]
              ->Array.mapWithIndex((j, disabled) => {
                  <div
                    className="flex items-center flex-col-gap-1 mb-2">
                    {sizes
                     ->Js.Dict.entries
                     ->Array.mapWithIndex((k, (sizeText, size)) => {
                         <div
                           key={
                             (i + j + k)->Int.toString ++ colorText ++ sizeText
                           }>
                           <Toolkit__Ui_Button
                             color size disabled variant=`text>
                             "hello"->React.string
                           </Toolkit__Ui_Button>
                         </div>
                       })
                     ->React.array}
                  </div>
                })
              ->React.array}
           </div>
         </div>
       })
     ->React.array}
  </div>;
};

textPalette->CSF.addMeta(
  ~name="text palette",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <Docs.Title />
          <Docs.Source
            language="reason"
            code={j|
            <Button color=`primary variant=`text>"Content"->React.string</Button>
          |j}
          />
          <Toolkit__Ui_Button color=`primary variant=`text>
            "hello"->React.string
          </Toolkit__Ui_Button>
        </>,
    },
  },
  (),
);

let loading = () =>
  <Toolkit__Ui_Button color=`primary isLoading=true>
    "content"->React.string
  </Toolkit__Ui_Button>;

loading->CSF.addMeta(
  ~name="loading",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <Docs.Title />
          <Docs.Source
            language="reason"
            code={j|
            <Button color=`primary isLoading=true>"Content"->React.string</Button>
          |j}
          />
          <Docs.Story id="components-button--loading" />
        </>,
    },
  },
  (),
);

let disabled = () =>
  <Toolkit__Ui_Button color=`primary disabled=true>
    "content"->React.string
  </Toolkit__Ui_Button>;

disabled->CSF.addMeta(
  ~name="disabled",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <Docs.Title />
          <Docs.Source
            language="reason"
            code={j|
            <Button color=`primary disabled=true>"Content"->React.string</Button>
          |j}
          />
          <Docs.Story id="components-button--disabled" />
        </>,
    },
  },
  (),
);
