open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Modal.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Components/Modal", ());

let defaultExample = () => {
  let modalSize = [|("lg", `lg), ("md", `md), ("sm", `sm), ("xs", `xs)|];

  let (isVisible, setVisibility) = React.useState(() => false);
  let (size, setSize) = React.useState(() => `md);

  <div>
    <div className="mb-4">
      <label htmlFor="size"> "Size :"->React.string </label>
      <select
        id="size"
        onChange={event => {
          let value = event->ReactEvent.Form.target##value;

          setSize(_ => value);
        }}>
        {modalSize
         ->Array.map(((label, value)) => {
             <option value={value->Obj.magic} selected={value == size}>
               label->React.string
             </option>
           })
         ->React.array}
      </select>
    </div>
    <Button onClick={_ => setVisibility(_ => true)}>
      "Show modal"->React.string
    </Button>
    <Modal
      isVisible
      title={"title"->React.string}
      body={"Body"->React.string}
      hide={() => setVisibility(_ => false)}
      size
    />
  </div>;
};

defaultExample->CSF.addMeta(
  ~name="default",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
let (isVisible, setVisibility) = React.useState(() => false);

<>
  <button onClick={_ => setVisibility(_ => true)}>
    "Show modal"->React.string
  </button>
  <Modal
    isVisible
    title={"title"->React.string}
    body={"Body"->React.string}
    hide={() => setVisibility(_ => false)}
  />
</>;
          |j}
          />
          <Docs.Story id="components-modal--default" />
        </>,
    },
  },
  (),
);

let coloredExample = () => {
  let modalSize = [|("lg", `lg), ("md", `md), ("sm", `sm), ("xs", `xs)|];

  let modalColor: array((string, Modal.color)) = [|
    ("primary", `primary),
    ("success", `success),
    ("danger", `danger),
    ("neutral", `neutral),
  |];

  let (isVisible, setVisibility) = React.useState(() => false);
  let (size, setSize) = React.useState(() => `md);
  let (color, setColor) = React.useState(() => `primary);

  <div>
    <div>
      <label htmlFor="size"> "Size :"->React.string </label>
      <select
        id="size"
        onChange={event => {
          let value = event->ReactEvent.Form.target##value;

          setSize(_ => value);
        }}>
        {modalSize
         ->Array.map(((label, value)) => {
             <option value={value->Obj.magic} selected={value == size}>
               label->React.string
             </option>
           })
         ->React.array}
      </select>
    </div>
    <div>
      <label htmlFor="color"> "color :"->React.string </label>
      <select
        onChange={event => {
          let value = event->ReactEvent.Form.target##value;

          setColor(_ => value);
        }}>
        {modalColor
         ->Array.map(((label, value)) => {
             <option value={value->Obj.magic} selected={value == color}>
               label->React.string
             </option>
           })
         ->React.array}
      </select>
    </div>
    <Button onClick={_ => setVisibility(_ => true)}>
      "Show modal"->React.string
    </Button>
    <Modal
      isVisible
      title={"title"->React.string}
      body={"Body"->React.string}
      hide={() => setVisibility(_ => false)}
      size
      type_={`colored(color)}
    />
  </div>;
};

coloredExample->CSF.addMeta(
  ~name="colored",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
let modalSize = [|("lg", `lg), ("md", `md), ("sm", `sm), ("xs", `xs)|];

let modalColor: array((string, Modal.color)) = [|
  ("primary", `primary),
  ("success", `success),
  ("danger", `danger),
  ("neutral", `neutral),
|];

let (isVisible, setVisibility) = React.useState(() => false);
let (size, setSize) = React.useState(() => `md);
let (color, setColor) = React.useState(() => `primary);

<div>
  <div>
    <label htmlFor="size"> "Size :"->React.string </label>
    <select
      id="size"
      onChange={event => {
        let value = event->ReactEvent.Form.target##value;

        setSize(_ => value);
      }}>
      {modalSize
        ->Array.map(((label, value)) => {
            <option value={value->Obj.magic} selected={value == size}>
              label->React.string
            </option>
          })
        ->React.array}
    </select>
  </div>
  <div>
    <label htmlFor="color"> "color :"->React.string </label>
    <select
      onChange={event => {
        let value = event->ReactEvent.Form.target##value;

        setColor(_ => value);
      }}>
      {modalColor
        ->Array.map(((label, value)) => {
            <option value={value->Obj.magic} selected={value == color}>
              label->React.string
            </option>
          })
        ->React.array}
    </select>
  </div>
  <Button onClick={_ => setVisibility(_ => true)}>
    "Show modal"->React.string
  </Button>
  <Modal
    isVisible
    title={"title"->React.string}
    body={"Body"->React.string}
    hide={() => setVisibility(_ => false)}
    size
    type_={`colored(color)}
  />
</div>;
          |j}
          />
          <Docs.Story id="components-modal--colored" />
        </>,
    },
  },
  (),
);
