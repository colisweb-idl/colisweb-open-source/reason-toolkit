open BsStorybook;
open Storybook;
open Toolkit__Ui;

// [@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Label.rei"]
// external source: 'a = "default";

let default = CSF.make(~title="Form/TextInput", ());

let sample = () =>
  <div>
    <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
      "Label"->React.string
    </Label>
    <TextInput id="test" placeholder="Test" />
  </div>;

sample->CSF.addMeta(
  ~name="sample",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          // <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<div>
  <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
    "Label"->React.string
  </Label>
  <TextInput id="test" placeholder="Test" />
</div>
          |j}
          />
          <Docs.Story id="form-textinput--sample" />
        </>,
    },
  },
  (),
);

let withError = () =>
  <div>
    <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
      "Label"->React.string
    </Label>
    <TextInput id="test" placeholder="Test" isInvalid=true />
  </div>;

withError->CSF.addMeta(
  ~name="withError",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          // <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<div>
  <Label htmlFor="test" optionalMessage={"Optional"->React.string}>
    "Label"->React.string
  </Label>
  <TextInput id="test" placeholder="Test" isInvalid=true />
</div>;
          |j}
          />
          <Docs.Story id="form-textinput--with-error" />
        </>,
    },
  },
  (),
);
