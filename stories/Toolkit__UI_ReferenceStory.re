open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Reference.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Components/Reference", ());

let defaultExample = () =>
  <div>
    <Reference reference="07868-9897687-0898" />
    <div className="w-48 mt-4">
      <Reference reference="07868-9897687-0898" />
    </div>
  </div>;

defaultExample->CSF.addMeta(
  ~name="default",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<div>
  <Reference reference="07868-9897687-0898" />
  <div className="w-48 mt-4">
    <Reference reference="07868-9897687-0898" />
  </div>
</div>;
          |j}
          />
          <Docs.Story id="components-reference--default-example" />
        </>,
    },
  },
  (),
);
