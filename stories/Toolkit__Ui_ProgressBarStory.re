open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_ProgressBar.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Components/ProgressBar", ());

let defaultPalette = () => {
  let colors =
    Js.Dict.fromArray([|
      ("success", `success),
      ("warning", `warning),
      ("info", `info),
      ("danger", `danger),
    |]);

  <div>
    {colors
     ->Js.Dict.entries
     ->Array.mapWithIndex((i, (colorText, color)) => {
         <div
           key={i->Int.toString ++ colorText}
           className="flex flex-row flex-col-gap-4 mb-4 items-center">
           <strong className="w-40">
             colorText->React.string
           </strong>
           <div className="w-60">
             <ProgressBar color progression=60. />
           </div>
         </div>
       })
     ->React.array}
  </div>;
};

defaultPalette->CSF.addMeta(
  ~name="default palette",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reason"
            code={j|
            <ProgressBar color=`success progression=60. />
          |j}
          />
          <ProgressBar color=`success progression=60. />
        </>,
    },
  },
  (),
);
