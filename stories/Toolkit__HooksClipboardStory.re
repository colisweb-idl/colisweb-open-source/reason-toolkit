open BsStorybook;
open Storybook;

let default = CSF.make(~title="Hooks/useClipboard", ());

let useClipboard = () => {
  let clipboard =
    Toolkit.Hooks.useClipboard(
      ~onCopyNotificationMessage="Copied !",
      "copied content",
    );

  <>
    <Toolkit.Ui.Snackbar.Provider />
    <Toolkit.Ui.Button onClick={_ => clipboard.copy()}>
      "Copy"->React.string
    </Toolkit.Ui.Button>
  </>;
};

useClipboard->CSF.addMeta(
  ~name="example",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <Docs.Source
            language="reasonml"
            code={j|
let clipboard =
  Toolkit.Hooks.useClipboard(
    ~onCopyNotificationMessage="Copied !",
    "copied content",
  );

<>
  <Toolkit.Ui.Snackbar.Provider />
  <Toolkit.Ui.Button onClick={_ => clipboard.copy()}>
    "Copy"->React.string
  </Toolkit.Ui.Button>
</>;
          |j}
          />
          <Docs.Story id="hooks-useclipboard--use-clipboard" />
        </>,
    },
  },
  (),
);
