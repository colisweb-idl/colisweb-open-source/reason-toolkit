open BsStorybook.Story;
open Storybook;

open ReachUi;

let _module = [%bs.raw "module"];

storiesOf("Components/Tabs", _module)
->addDecorator(Knobs.withKnobs)
->add("default", () => {
    <div>
      <Tabs className="flex flex-col">
        <TabList className="cw-Tabs">
          <Tab> "Tab 1"->React.string </Tab>
          <Tab> "Tab 2"->React.string </Tab>
        </TabList>
        <TabPanels className="mt-4">
          <TabPanel> "Panel 1"->React.string </TabPanel>
          <TabPanel> "Panel 2"->React.string </TabPanel>
        </TabPanels>
      </Tabs>
    </div>
  })
->add("big", () => {
    <div>
      <Tabs className="flex flex-col">
        <TabList className="cw-Tabs cw-Tabs--big">
          <Tab> "Tab 1"->React.string </Tab>
          <Tab> "Tab 2"->React.string </Tab>
        </TabList>
        <TabPanels className="mt-4">
          <TabPanel> "Panel 1"->React.string </TabPanel>
          <TabPanel> "Panel 2"->React.string </TabPanel>
        </TabPanels>
      </Tabs>
    </div>
  })
->add("with render", () => {
    <div>
      <TabsRender className="flex flex-col">
        {({selectedIndex}) => {
           <>
             {("selected index : " ++ selectedIndex->Int.toString)
              ->React.string}
             <TabList className="cw-Tabs">
               <Tab> "Tab 1"->React.string </Tab>
               <Tab> "Tab 2"->React.string </Tab>
             </TabList>
             <TabPanels className="mt-4">
               <TabPanel> "Panel 1"->React.string </TabPanel>
               <TabPanel> "Panel 2"->React.string </TabPanel>
             </TabPanels>
           </>;
         }}
      </TabsRender>
    </div>
  })
->ignore;
