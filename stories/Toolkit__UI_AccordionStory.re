open BsStorybook.Story;
open Storybook;

open ReachUi;

let _module = [%bs.raw "module"];

module Example = {
  [@react.component]
  let make = () => {
    <div>
      <Accordion>
        <AccordionItem>
          "title"->React.string
          <AccordionButton>
            <span className="cw-accordion-icon">
              <BsReactIcons.MdKeyboardArrowRight />
            </span>
          </AccordionButton>
          <AccordionPanel> "content"->React.string </AccordionPanel>
        </AccordionItem>
        <AccordionItem>
          <AccordionButton className="flex items-center">
            "title"->React.string
            <span className="cw-accordion-icon">
              <BsReactIcons.MdKeyboardArrowRight />
            </span>
          </AccordionButton>
          <AccordionPanel> "content"->React.string </AccordionPanel>
        </AccordionItem>
      </Accordion>
    </div>;
  };
};

storiesOf("Components/Accordion", _module)
->addDecorator(Knobs.withKnobs)
->add("default", () => {<Example />})
->ignore;
