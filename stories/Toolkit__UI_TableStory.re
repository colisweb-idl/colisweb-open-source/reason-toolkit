open BsStorybook;
open Storybook;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Table.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Components/Table", ());

type data = array((test, toto))
and test = {
  name: string,
  age: int,
}
and toto = Js.Dict.t(string);

module Test = {
  let data: data =
    Array.make(100, ({name: "toto", age: 2}, Js.Dict.empty()));

  [@react.component]
  let make = () => {
    let columns =
      React.useMemo0(() => {
        [|
          ReactTable.makeColumn(
            ~accessor=((value, _dict)) => value.name,
            ~id="test",
            ~header="Name"->React.string,
            ~filterRender=_ => React.null,
            ~cell=cell => {<p> cell.cell.value->React.string </p>},
            (),
          ),
        |]
      });

    let table =
      ReactTable.(
        useTable5(
          make(~columns, ~data, ()),
          useFilters,
          useSortBy,
          usePagination,
          useAbsoluteLayout,
          useResizeColumns,
        )
      );

    <div>
      <Toolkit__Ui_Table.Pagination table />
      <Toolkit__Ui_Table.Core table emptyMessage="No items" />
    </div>;
  };
};

let example = () => {
  <Test />;
};

example->CSF.addMeta(
  ~name="example",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
let data: data =
    Array.make(100, ({name: "toto", age: 2}, Js.Dict.empty()));

  [@react.component]
  let make = () => {
    let columns =
      React.useMemo0(() => {
        [|
          ReactTable.makeColumn(
            ~accessor=((value, _dict)) => value.name,
            ~id="test",
            ~header="Name"->React.string,
            ~filterRender=_ => React.null,
            ~cell=cell => {<p> cell.cell.value->React.string </p>},
            (),
          ),
        |]
      });

    let table =
      ReactTable.(
        useTable5(
          make(~columns, ~data, ()),
          useFilters,
          useSortBy,
          usePagination,
          useAbsoluteLayout,
          useResizeColumns,
        )
      );

    <div>
      <Toolkit__Ui_Table.Pagination table />
      <Toolkit__Ui_Table.Core table emptyMessage="No items" />
    </
          |j}
          />
          <Docs.Story id="components-table--example" />
        </>,
    },
  },
  (),
);
