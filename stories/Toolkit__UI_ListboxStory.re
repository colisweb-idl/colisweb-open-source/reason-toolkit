open BsStorybook.Story;
open Storybook;
open Toolkit__Ui;

let _module = [%bs.raw "module"];

let items = [|("label", "value"), ("label2", "value2"), ("label3", "value3"),|];

storiesOf("Components/Listbox", _module)
->addDecorator(Knobs.withKnobs)
->add("multiple", () => {
    <span>
      <ListboxInput.MultiSelect
        items
        name="name"
        itemToLabel={((label, _)) => label}
        itemToId={((_, v)) => v}
        id="id"
        placeholder="Placeholder"
        onChange={_ => ()}
        onBlur={_ => ()}
      />
      <br />
      <div className="w-40">
        <ListboxInput.MultiSelect
          items
          name="name"
          itemToLabel={((label, _)) => label}
          itemToId={((_, v)) => v}
          id="id"
          placeholder="Placeholder"
          onChange={_ => ()}
          onBlur={_ => ()}
        />
      </div>
    </span>
  })->ignore;
