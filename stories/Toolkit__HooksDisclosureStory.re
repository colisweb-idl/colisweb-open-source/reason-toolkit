open BsStorybook;
open Storybook;

let default = CSF.make(~title="Hooks/useDisclosure", ());

let useDisclosure = () => {
  let disclosure = Toolkit.Hooks.useDisclosure();

  <div className="grid grid-cols-2 gap-4 w-64">
    <div className="grid grid-rows-3 gap-4">
      <Toolkit.Ui.Button onClick={_ => disclosure.toggle()}>
        "Toggle"->React.string
      </Toolkit.Ui.Button>
      <Toolkit.Ui.Button onClick={_ => disclosure.show()}>
        "Show"->React.string
      </Toolkit.Ui.Button>
      <Toolkit.Ui.Button onClick={_ => disclosure.hide()}>
        "Hide"->React.string
      </Toolkit.Ui.Button>
    </div>
    <p> (disclosure.isOpen ? "Visible" : "hidden")->React.string </p>
  </div>;
};

useDisclosure->CSF.addMeta(
  ~name="example",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          // <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
let disclosure = Toolkit.Hooks.useDisclosure();

<div className="grid grid-cols-2 gap-4 w-64">
  <div className="grid grid-rows-3 gap-4">
    <Toolkit.Ui.Button onClick={_ => disclosure.toggle()}>
      "Toggle"->React.string
    </Toolkit.Ui.Button>
    <Toolkit.Ui.Button onClick={_ => disclosure.show()}>
      "Show"->React.string
    </Toolkit.Ui.Button>
    <Toolkit.Ui.Button onClick={_ => disclosure.hide()}>
      "Hide"->React.string
    </Toolkit.Ui.Button>
  </div>
  <p> (disclosure.isOpen ? "Visible" : "hidden")->React.string </p>
</div>;
          |j}
          />
          <Docs.Story id="hooks-usedisclosure--use-disclosure" />
        </>,
    },
  },
  (),
);
