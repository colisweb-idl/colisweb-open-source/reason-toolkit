open BsStorybook;
open Storybook;
open Toolkit__Ui;

[@bs.module "!!raw-loader!../../../src/ui/Toolkit__Ui_Switch.rei"]
external source: 'a = "default";

let default = CSF.make(~title="Form/Switch", ());

let sample = () => {
  <div> <Switch size=`md /> <Switch size=`lg /> </div>;
};

sample->CSF.addMeta(
  ~name="sample",
  ~parameters={
    "docs": {
      "page": () =>
        <>
          <CodeBlock> source </CodeBlock>
          <Docs.Source
            language="reasonml"
            code={j|
<div>
  <Switch size=`md />
  <Switch size=`lg />
</div>
          |j}
          />
          <Docs.Story id="form-switch--sample" />
        </>,
    },
  },
  (),
);
