import React, { useEffect } from "react";
import hljs from "highlight.js/lib/core";
import reasonml from "./utils/reason-highlight";
import bash from "highlight.js/lib/languages/bash";
import js from "highlight.js/lib/languages/javascript";
// import "highlight.js/styles/github.css";
hljs.registerLanguage("reasonml", reasonml);
hljs.registerLanguage("bash", bash);
hljs.registerLanguage("js", js);

export default function Code({ className, children }) {
  useEffect(() => {
    hljs.highlightAll();
  }, []);

  return (
    <div className="Code border rounded-lg mb-6 relative">
      <pre className="p-4">
        <code className={className}>{children}</code>
      </pre>
      <button
        className="absolute right-0 bottom-0 bg-white rounded-tl rounded-br text-sm px-2 border-t border-l hover:bg-neutral-200"
        onClick={(_) => navigator.clipboard.writeText(children)}
      >
        Copy
      </button>
    </div>
  );
}
