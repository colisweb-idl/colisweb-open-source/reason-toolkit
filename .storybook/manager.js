import { addons } from "@storybook/addons";
import { themes, create } from "@storybook/theming";

const theme = create({
  base: "light",
  brandTitle: "Colisweb Toolkit",
  brandUrl: "https://colisweb.com",
  brandImage:
    "https://images.prismic.io/colisweb-corporate/8394ac5be96e7a0ecfc6a282c1a21de1615998ca_logo_c_bleu-1.png?auto=compress,format",
  fontCode: "SFMono-Regular, Roboto Mono, Menlo, Segoe UI, Courier, monospace",
});

addons.setConfig({
  theme,
});
