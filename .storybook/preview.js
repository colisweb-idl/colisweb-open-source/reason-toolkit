import "../src/ui/styles.css";
import "./custom.css";
import CodeBlock from "./CodeBlock";

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  options: {
    storySort: {
      method: "alphabetical",
      order: [
        "Setup",
        "Documentation",
        "Tailwind",
        "Components",
        "Form",
        "Hooks",
      ],
    },
  },
  docs: {
    components: {
      code: CodeBlock,
      Source: CodeBlock,
    },
  },
};
