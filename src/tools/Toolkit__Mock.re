// Setup faker and json-schema-faker
// To set faker locale:
//    ref(jsf##locate("faker")##locale) := "fr";
let jsf = [%bs.raw
  {|
(function () {
  var faker = require("faker");
  var jsf = require("json-schema-faker");

  jsf.define("faker", function (value, schema) {
    var data;
    var getFakerFn = function (path) {
      return path.split(".").reduce(function (acc, part) {
        return acc[part];
      }, faker);
    };

    // Handle nullable
    if (Array.isArray(schema.type) && schema.type.includes("null") && faker.random.boolean()) {
      return null;
    }

    // Generate fake data
    if (typeof value === "object") {
      var key = Object.keys(value)[0];
      data = getFakerFn(key).apply(null, [].concat(value[key]));
    } else {
      data = getFakerFn(value)();
    }

    // Cast data
    var shouldCastToString = schema.type === "string" || Array.isArray(schema.type) && schema.type.includes("string");
    if (shouldCastToString) return "" + data;

    // TODO: cast number

    return data;
  })

  jsf.option("alwaysFakeOptionals", true);
  faker.locale = "fr";
  faker.seed(123);

  return jsf;
})()
|}
];

type resolver =
  (
    Msw.request,
    Msw.response,
    Msw.context,
    unit => Js.Promise.t(Js.Null.t(Msw.mockedResponse))
  ) =>
  Js.Promise.t(Js.Null.t(Msw.mockedResponse));

module SchemaParser = {
  [@bs.module "json-schema-ref-parser"]
  external dereference: Js.t('a) => Js.Promise.t(Js.t('a)) = "dereference";

  type jsonSchemaOptions;
  [@bs.obj]
  external jsonSchemaOptions:
    (~keepNotSupported: array(string)=?, ~cloneSchema: bool=?, unit) =>
    jsonSchemaOptions;

  type jsonSchema;
  [@bs.module]
  external jsonSchema: (Js.t('a), jsonSchemaOptions) => jsonSchema =
    "@openapi-contrib/openapi-schema-to-json-schema";

  type value;

  type response = {
    id: string,
    status: string,
    content: option(string),
    name: option(string),
    value: option(value),
    schema: option(jsonSchema),
  };

  type operation = {
    id: string,
    method: string,
    path: string,
    responses: array(response),
    activeResponse: option(string),
    delayed: bool,
  };

  type schema = {
    id: string,
    title: string,
    version: string,
    operations: array(operation),
    url: string,
    resolver: option(resolver),
  };

  let getActiveResponse = ({activeResponse, responses}): option(response) => {
    switch (activeResponse) {
    | None => None
    | Some(activeResponse) =>
      responses->Array.getBy(({id}: response) => id === activeResponse)
    };
  };

  let parseResponses = responses => {
    let response = (~status, ~content=?, ~name=?, ~value=?, ~schema=?, ()) => {
      let content =
        switch (content) {
        | Some("application/json") => Some("json")
        | Some("text/xml") => Some("xml")
        | Some("text/plain") => Some("text")
        | None => None
        | Some(content) =>
          Js.Exn.raiseError({j|Invalid content "$content"|j})
        };

      let schema =
        switch (schema) {
        | None => None
        | Some(schema) => Some(jsonSchema(schema, jsonSchemaOptions()))
        };

      let contentId = content->Option.getWithDefault("empty");

      {
        id: {j|$status-$contentId-$name|j},
        status,
        content,
        name,
        value,
        schema,
      };
    };

    switch (responses) {
    | None => [||]
    | Some(responses) =>
      Js.Dict.entries(responses)
      ->Array.reduce(
          [||],
          (acc, (status, statusValue)) => {
            let statusValue = statusValue->Js.Nullable.toOption;

            acc->Array.concat(
              switch (statusValue) {
              | None => [||]
              | Some(statusValue) =>
                let content = statusValue##content->Js.Nullable.toOption;
                switch (content) {
                | None => [|response(~status, ~name=status, ())|]
                | Some(content) =>
                  Js.Dict.entries(content)
                  ->Array.reduce(
                      [||],
                      (acc, (content, contentValue)) => {
                        let contentValue = contentValue->Js.Nullable.toOption;

                        acc->Array.concat(
                          switch (contentValue) {
                          | None => [|response(~status, ())|]
                          | Some(contentValue) =>
                            let examples =
                              contentValue##examples->Js.Nullable.toOption;

                            let example =
                              contentValue##example->Js.Nullable.toOption;

                            let dynamicExample =
                              response(
                                ~status,
                                ~content,
                                ~name="dynamic",
                                ~schema=contentValue##schema,
                                (),
                              );

                            let staticExample =
                              switch (example) {
                              | None => None
                              | Some(example) =>
                                Some(
                                  response(
                                    ~status,
                                    ~content,
                                    ~name="static",
                                    ~schema=contentValue##schema,
                                    ~value=example,
                                    (),
                                  ),
                                )
                              };

                            let defaultExamples =
                              [|dynamicExample|]
                              ->Array.concat(
                                  switch (staticExample) {
                                  | None => [||]
                                  | Some(staticExample) => [|staticExample|]
                                  },
                                );

                            switch (examples) {
                            | None => defaultExamples
                            | Some(examples) =>
                              Js.Dict.entries(examples)
                              ->Array.reduce(
                                  defaultExamples,
                                  (acc, (name, exampleValue)) => {
                                  acc->Array.concat([|
                                    response(
                                      ~status,
                                      ~content,
                                      ~name,
                                      ~schema=contentValue##schema,
                                      ~value=exampleValue##value,
                                      (),
                                    ),
                                  |])
                                })
                            };
                          },
                        );
                      },
                    )
                };
              },
            );
          },
        )
    };
  };

  let parsePaths = (paths: Js.Nullable.t(Js.Dict.t('a))) => {
    let paths = paths->Js.Nullable.toOption;

    switch (paths) {
    | None => [||]
    | Some(paths) =>
      Js.Dict.entries(paths)
      ->Array.reduce(
          [||],
          (acc, (path, pathValue)) => {
            let path =
              path |> Js.String.replaceByRe([%re "/\\{(\\w+)\\}/g"], ":$1");

            let pathValue = pathValue->Js.Nullable.toOption;

            acc->Array.concat(
              switch (pathValue) {
              | None => [||]
              | Some(pathValue) =>
                Js.Dict.entries(pathValue)
                ->Array.reduce(
                    [||],
                    (acc, (method, methodValue)) => {
                      let methodValue = methodValue->Js.Nullable.toOption;

                      let responses =
                        switch (methodValue) {
                        | None => [||]
                        | Some(methodValue) =>
                          parseResponses(
                            methodValue##responses->Js.Nullable.toOption,
                          )
                        };

                      acc->Array.concat([|
                        {
                          id: {j|$method_-$path|j},
                          method,
                          path,
                          responses,
                          delayed: false,
                          activeResponse:
                            switch (responses->Belt.Array.get(0)) {
                            | None => None
                            | Some({id}) => Some(id)
                            },
                        },
                      |]);
                    },
                  )
              },
            );
          },
        )
    };
  };

  let parseSchema = (schema: Js.t('a), url, resolver): Js.Promise.t(schema) => {
    dereference(schema)
    |> Js.Promise.then_(schema => {
         let {title, version} = schema##info;
         let operations = parsePaths(schema##paths);
         Js.Promise.resolve({
           id: {j|$title-$version|j},
           title,
           version,
           operations,
           url,
           resolver,
         });
       });
  };
};

let getHandlers = (schema: SchemaParser.schema) => {
  let {url, resolver}: SchemaParser.schema = schema;
  schema.operations
  ->Array.map(operation => {
      let {method, path, delayed}: SchemaParser.operation = operation;
      switch (Msw.restDict->Js.Dict.get(method->Js.String.toLowerCase)) {
      | None => Js.Exn.raiseError({j|Invalid method "$method_"|j})
      | Some(fn) =>
        fn(url ++ path, (req, res, ctx) => {
          switch (SchemaParser.getActiveResponse(operation)) {
          | None => Js.Promise.resolve(Js.null)
          | Some({status, content, schema, value}) =>
            let next = () =>
              switch (content) {
              | None =>
                let response = () =>
                  Js.Promise.resolve(
                    Msw.response(
                      res,
                      [|ctx->Msw.status(status)|]
                      ->Js.Array.concat(
                          delayed ? [|ctx->Msw.delay(2000)|] : [||],
                        ),
                    ),
                  );
                switch (resolver) {
                | None => response()
                | Some(resolver) => resolver(req, res, ctx, response)
                };
              | Some(content) =>
                let data =
                  switch (schema, value) {
                  | (_, Some(value)) => value->Obj.magic
                  | (Some(schema), _) => jsf##generate(schema)
                  | _ => Js.Obj.empty()
                  };

                let response = () =>
                  Js.Promise.resolve(
                    Msw.response(
                      res,
                      [|
                        ctx->Msw.status(status),
                        switch (content) {
                        | "json" => ctx->Msw.json(data)
                        | "xml" => ctx->Msw.xml(data)
                        | "text" => ctx->Msw.text(data)
                        | _ =>
                          Js.Exn.raiseError({j|Invalid content "$content"|j})
                        },
                      |]
                      ->Js.Array.concat(
                          delayed ? [|ctx->Msw.delay(2000)|] : [||],
                        ),
                    ),
                  );

                switch (resolver) {
                | None => response()
                | Some(resolver) => resolver(req, res, ctx, response)
                };
              };

            next();
          }
        })
      };
    });
};

module Cache = {
  [@bs.val] [@bs.scope "localStorage"]
  external getItem: string => string = "getItem";

  [@bs.val] [@bs.scope "localStorage"]
  external setItem: (string, string) => unit = "setItem";

  let get = () =>
    (
      try(Some(Js.Json.parseExn(getItem("mock-monitor"))->Obj.magic)) {
      | _ => None
      }
    )
    ->Js.Nullable.fromOption;

  let set = (schemas: array(SchemaParser.schema)) => {
    let cache = Js.Dict.empty();
    schemas->Array.forEach(({id: schemaId, operations}) => {
      operations->Array.forEach(({id: operationId, activeResponse, delayed}) => {
        cache->Js.Dict.set(
          {j|$schemaId-$operationId|j},
          (activeResponse->Option.getWithDefault(""), delayed),
        )
      })
    });
    setItem("mock-monitor", cache->Obj.magic->Js.Json.stringify);
  };
};

type mockOptions('a) = {
  url: string,
  schema: Js.t('a),
  resolver: option(resolver),
};

[@bs.obj]
external mockOptions:
  (~url: string, ~schema: Js.t('a), ~resolver: resolver=?, unit) =>
  mockOptions('a);

module Monitor = {
  [@react.component]
  let make =
      (
        ~schemas: array(mockOptions('a)),
        ~onChange: array(SchemaParser.schema) => unit,
        ~worker: Msw.worker,
      ) => {
    let options = schemas;
    let (schemas, setSchemas) = React.useState(() => None);
    let setSchemas = schemas => {
      let handlers =
        schemas
        ->Array.map(schema => getHandlers(schema))
        ->Array.reduce([||], (acc, handlers) => acc->Array.concat(handlers));
      worker->Msw.resetHandlers(handlers);
      Cache.set(schemas);
      onChange(schemas);
      setSchemas(_ => Some(schemas));
    };
    let (isVisible, setIsVisible) = React.useState(() => false);
    let (counterToggle, setCounterToggle) = React.useState(() => 1);

    React.useEffect1(
      () => {
        let cache = Cache.get();

        Js.Promise.all(
          options->Array.map(({url, schema, resolver}) =>
            SchemaParser.parseSchema(schema, url, resolver)
            |> Js.Promise.then_((schema: SchemaParser.schema) => {
                 let schemaId = schema.id;
                 Js.Promise.resolve({
                   ...schema,
                   operations:
                     schema.operations
                     ->Array.map(operation => {
                         let operationId = operation.id;
                         let (activeResponse, delayed) = {
                           let default = (
                             operation.activeResponse,
                             operation.delayed,
                           );
                           cache
                           ->Js.Nullable.toOption
                           ->Option.mapWithDefault(default, cache => {
                               cache
                               ->Js.Dict.get({j|$schemaId-$operationId|j})
                               ->Option.getWithDefault(default)
                             });
                         };

                         {...operation, activeResponse, delayed};
                       }),
                 });
               })
          ),
        )
        |> Js.Promise.then_(schemas => {
             setSchemas(schemas);
             Js.Promise.resolve();
           })
        |> ignore;

        None;
      },
      [|options|],
    );

    let setDelayedResponse = (schemaId, operationId, delayed) => {
      setSchemas(
        schemas
        ->Option.getWithDefault([||])
        ->Array.map(schema =>
            {
              ...schema,
              operations: {
                schema.id === schemaId
                  ? schema.operations
                    ->Array.map(operation => {
                        operation.id === operationId
                          ? {...operation, delayed} : operation
                      })
                  : schema.operations;
              },
            }
          ),
      );
    };

    let setActiveResponse = (schemaId, operationId, responseId) => {
      setSchemas(
        schemas
        ->Option.getWithDefault([||])
        ->Array.map(schema =>
            {
              ...schema,
              operations: {
                schema.id === schemaId
                  ? schema.operations
                    ->Array.map(operation => {
                        operation.id === operationId
                          ? {...operation, activeResponse: responseId}
                          : operation
                      })
                  : schema.operations;
              },
            }
          ),
      );
    };

    let setAllActiveResponses = isActive => {
      setSchemas(
        schemas
        ->Option.getWithDefault([||])
        ->Array.map(schema =>
            {
              ...schema,
              operations: {
                schema.operations
                ->Array.map(operation => {
                    {
                      ...operation,
                      activeResponse:
                        switch (
                          isActive,
                          operation.responses->Belt.Array.get(0),
                        ) {
                        | (true, Some({id})) => Some(id)
                        | _ => None
                        },
                    }
                  });
              },
            }
          ),
      );
    };

    isVisible
      ? <div
          className="fixed top-0 right-0 z-50 bg-white shadow-lg rounded border-2 border-warning-500">
          <div
            className="px-4 text-info-500 font-semibold flex justify-end border-b text-sm">
            <button
              className="p-1 hover:underline"
              onClick={_ =>
                setCounterToggle(counter => {
                  setAllActiveResponses(counter mod 2 === 0);
                  let counter = counter + 1;
                  counter;
                })
              }>
              {counterToggle mod 2 === 0 ? "Enable all" : "Disable all"}
              ->React.string
            </button>
            <button
              className="p-1 hover:underline"
              onClick={_ => setIsVisible(_ => false)}>
              "Close"->React.string
            </button>
          </div>
          <div className="overflow-y-auto h-screen">
            {schemas
             ->Option.getWithDefault([||])
             ->Array.map(({id: schemaId, title, version, operations}) =>
                 <React.Fragment key=schemaId>
                   <div className="px-4 py-2 bg-gray-100 flex justify-between">
                     <div className="font-semibold text-sm">
                       title->React.string
                       <span
                         className="text-xs text-gray-700 ml-2 inline-block">
                         version->React.string
                       </span>
                     </div>
                     <div className="flex">
                       <div className="text-sm text-gray-700 mr-2">
                         "Delay"->React.string
                       </div>
                       <div className="w-48 text-sm text-gray-700">
                         "Response"->React.string
                       </div>
                     </div>
                   </div>
                   <ul>
                     {operations
                      ->Array.map(
                          (
                            {
                              id: operationId,
                              method,
                              path,
                              responses,
                              activeResponse,
                              delayed,
                            },
                          ) => {
                          <li
                            key=operationId
                            className="px-4 py-2 flex justify-between">
                            <div className="truncate mr-2 max-w-md">
                              <span
                                className="text-gray-600 inline-block w-12 text-sm">
                                method->React.string
                              </span>
                              path->React.string
                            </div>
                            <div className="flex items-center">
                              <input
                                type_="checkbox"
                                className="mr-2"
                                checked=delayed
                                onChange={event => {
                                  let delayed =
                                    event->ReactEvent.Form.target##checked;

                                  setDelayedResponse(
                                    schemaId,
                                    operationId,
                                    delayed,
                                  );
                                }}
                              />
                              <select
                                className="w-48 truncate"
                                value={
                                  activeResponse->Option.getWithDefault("")
                                }
                                onChange={event => {
                                  let responseId =
                                    event->ReactEvent.Form.target##value;

                                  setActiveResponse(
                                    schemaId,
                                    operationId,
                                    responseId === ""
                                      ? None : Some(responseId),
                                  );
                                }}>
                                <option value="">
                                  "Disabled"->React.string
                                </option>
                                {responses
                                 ->Array.map(({id}) =>
                                     <option key=id value=id>
                                       id->React.string
                                     </option>
                                   )
                                 ->React.array}
                              </select>
                            </div>
                          </li>
                        })
                      ->React.array}
                   </ul>
                 </React.Fragment>
               )
             ->React.array}
          </div>
        </div>
      : <button
          className="fixed top-0 right-0 z-50 bg-warning-500 text-warning-700 font-bold shadow-lg p-2 rounded-full text-xs mt-px"
          onClick={_ => setIsVisible(_ => true)}>
          "Mock"->React.string
        </button>;
  };
};
