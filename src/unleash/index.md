# Unleash

[Unleash](https://github.com/Unleash/unleash) is feature flipping solution. It allows us to activate or restrain fonctionalities acording a configuration.

## Usage

### Create a rule

```reasonml
module OutdatedApplicationConfig = {
  [@decco]
  type parameters = {version: string};
  type input = unit;
  type output = bool;
  let envUrl = Env.unleashUrl;
  let featureName = "minimum-supported-version";

  let exec = (_, config: Unleash.config(parameters)) => {
    let appVersion =
      ReactNativeVersionNumber.appVersion
      ->Js.Nullable.toOption
      ->Option.getWithDefault("");

    config.strategies
    ->Array.get(0)
    ->Option.mapWithDefault(false, strategy =>
        config.enabled
          ? Semver.lt(appVersion, strategy.parameters.version) : false
      );
  };
};

module OutdatedApplication = Toolkit.Unleash.MakeFeature(OutdatedApplicationConfig);


/**
 * Later in app
 **/

OutdatedApplication.exec()
->Promise.tapOk(outdated => {
  if (outdated) {
    Js.log("app outdated");
  }
})
```
