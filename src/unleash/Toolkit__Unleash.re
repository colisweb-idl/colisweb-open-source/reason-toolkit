[@decco]
type strategy('parameters) = {
  name: string,
  parameters: 'parameters,
};

[@decco]
type config('strategy) = {
  name: string,
  description: string,
  enabled: bool,
  strategies: array(strategy('strategy)),
};

module type Config = {
  [@decco]
  type argument;
  type input;
  type output;
  let envUrl: string;
  let featureName: string;
  let exec: (input, config(argument)) => output;
};

module MakeFeature = (C: Config) => {
  [@decco]
  type t = config(C.argument);

  let exec = (var: C.input) => {
    (
      Axios.get(C.envUrl ++ C.featureName)
      |> Js.Promise.then_(res => res##data->Js.Promise.resolve)
      |> Js.Promise.then_(data =>
           switch (t_decode(data)) {
           | Result.Ok(feat) => Js.Promise.resolve(feat)
           | Result.Error(_err) =>
             [%log.error "unleash decode"; ("err", _err)];
             Js.Promise.reject(
               Js.Exn.raiseError(C.featureName ++ " : parse error"),
             );
           }
         )
      |> Promise.Js.fromBsPromise
      |> Promise.Js.toResult
    )
    ->Promise.mapOk(C.exec(var));
  };
};
