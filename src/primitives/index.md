# Primitives

Utililties functions for primitives types :

- `option`
- `string`
- `array`

## API

### Option

```reasonml
let anEmptyString = "";

Primitives.Option.fromString(anEmptyString)
->Option.map(v => v ++ "test");
```

### Array

```reasonml
let variable = [|"abc", "dce", ""|];

Js.log(Primitives.Array.joinNonEmpty(anEmptyString)); // "abc, dce"
Js.log(Primitives.Array.joinNonEmpty(~separator="+", anEmptyString)); // "abc+dce"
Js.log(variable->Primitives.Array.tail); // ""
```

### Result

```reasonml
let result = Ok("test");
let test = Error("error");

Js.log(Primitives.Result.get(result)); // Some("test")
Js.log(Primitives.Result.get(test)); // None
```
