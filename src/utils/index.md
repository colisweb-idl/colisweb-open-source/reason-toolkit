# Utils

Some utilities types

## UnitMeasure

### Dimension

```reasonml
let mm = `mm(10.);

mm->Toolkit.Utils.UnitMeasure.Dimension.toString; // "10.00 mm"
```

### Weight

```reasonml
let kg = `kg(10.);

kg->Toolkit.Utils.UnitMeasure.Weight.toString; // "10.00 kg"
```
