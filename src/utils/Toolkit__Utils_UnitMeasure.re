module Weight = {
  type t = [ | `g(float) | `kg(float)];

  let toValue = value =>
    switch (value) {
    | `g(v)
    | `kg(v) => v
    };

  let display = (value, ~digits=2, ()) =>
    switch (value) {
    | `g(v) => v->Js.Float.toFixedWithPrecision(~digits) ++ " g"
    | `kg(v) => v->Js.Float.toFixedWithPrecision(~digits) ++ " kg"
    };

  let makeG = (value: float) => `g(value);
  let makeKg = (value: float) => `kg(value);
};

module Dimension = {
  type t = [
    | `mm(float)
    | `cm(float)
    | `dm(float)
    | `m(float)
    | `km(float)
  ];

  let toValue = value =>
    switch (value) {
    | `mm(v)
    | `cm(v)
    | `dm(v)
    | `m(v)
    | `km(v) => v
    };

  let toString = (value, ~digits=2, ()) => {
    Js.Float.(
      switch (value) {
      | `mm(v) => v->toFixedWithPrecision(~digits) ++ " mm"
      | `cm(v) => v->toFixedWithPrecision(~digits) ++ " cm"
      | `dm(v) => v->toFixedWithPrecision(~digits) ++ " dm"
      | `m(v) => v->toFixedWithPrecision(~digits) ++ " m"
      | `km(v) => v->toFixedWithPrecision(~digits) ++ " km"
      }
    );
  };

  let makeMm = (value: float) => `mm(value);
  let makeCm = (value: float) => `cm(value);
  let makeDm = (value: float) => `dm(value);
  let makeM = (value: float) => `m(value);
  let makeKm = (value: float) => `km(value);
};
