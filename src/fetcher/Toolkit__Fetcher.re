module type Config = {
  module Request: Toolkit__Request.Config;
  let key: Request.argument => array(string);
};

module Make = (Config: Config) => {
  let updateData =
      (key: Config.Request.argument, ~shouldRevalidate=false, ~data): unit =>
    Toolkit__Hooks.updateFetcherData(
      Config.key(key),
      data,
      shouldRevalidate,
    );

  let trigger = (key: Config.Request.argument): unit =>
    Toolkit__Hooks.triggerFetcher(Config.key(key));

  let key: Config.Request.argument => array(string) = Config.key;

  let use =
      (
        ~options: option(Toolkit__Hooks.fetcherOptions)=?,
        key: option(Config.Request.argument),
      )
      : Toolkit__Hooks.fetcher(Config.Request.response) => {
    Toolkit__Hooks.useFetcher(
      ~options?,
      key->Option.map(Config.key),
      () => {
        let key = key->Option.getExn;

        Config.Request.exec(key)->Promise.Js.fromResult;
      },
    );
  };

  let useOptional =
      (
        ~options: option(Toolkit__Hooks.fetcherOptions)=?,
        key: option(Config.Request.argument),
      )
      : Toolkit__Hooks.fetcher(option(Config.Request.response)) => {
    Toolkit__Hooks.useOptionalFetcher(
      ~options?,
      key->Option.map(Config.key),
      () => {
        let key = key->Option.getExn;

        Config.Request.exec(key)->Promise.Js.fromResult;
      },
    );
  };
};
