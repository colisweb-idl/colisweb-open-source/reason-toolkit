module Spinner = Toolkit__Ui_Spinner;
module Button = Toolkit__Ui_Button;

type variant = Button.variant;

type size = Button.size;

type color = Button.color;

let buttonStyle =
    (
      ~size: size=`md,
      ~variant: variant=`default,
      ~color: color=`white,
      ~disabled,
      (),
    )
    : string => {
  let sizeStyle =
    switch (size) {
    | `xs => "text-sm leading-4 p-1"
    | `sm => "text-sm leading-4 p-2"
    | `md => "text-base leading-5 p-4"
    | `lg => "text-lg leading-8 p-6"
    };

  let colorStyle = Button.colorStyle(~color, ~variant, ~disabled);

  let baseStyle = Button.baseStyle();

  let disabledStyle =
    "opacity-50 cursor-not-allowed shadow-none"->Cn.ifTrue(disabled);

  Cn.make([baseStyle, sizeStyle, colorStyle, disabledStyle]);
};

[@react.component]
let make =
  React.forwardRef(
    (
      ~size=`md,
      ~color=`white,
      ~variant=`default,
      ~onClick: option(ReactEvent.Mouse.t => unit)=?,
      ~onBlur: option(ReactEvent.Focus.t => unit)=?,
      ~onFocus: option(ReactEvent.Focus.t => unit)=?,
      ~disabled=false,
      ~tabIndex: option(int)=?,
      ~id: option(string)=?,
      ~type_="button",
      ~ariaLabel: option(string)=?,
      ~className="",
      ~icon: React.element,
      ref_,
    ) => {
    <button
      ref=?{ref_->Js.Nullable.toOption->Option.map(ReactDOMRe.Ref.domRef)}
      disabled
      type_
      ?id
      ?tabIndex
      ?onFocus
      ?onBlur
      ?onClick
      ?ariaLabel
      className={Cn.make([
        buttonStyle(~color, ~variant, ~size, ~disabled, ()),
        className,
      ])}>
      icon
    </button>
  });
