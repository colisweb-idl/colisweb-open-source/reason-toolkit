open ReachUi_Tooltip;

type placement = [ | `bottom | `right];

[@bs.val] external innerWidth: int = "window.innerWidth";
[@bs.val] external scrollX: int = "window.scrollX";
[@bs.val] external scrollY: int = "window.scrollY";

let centered = (triggerRect: triggerRect, tooltipRect: triggerRect) => {
  let triggerCenter = triggerRect.left + triggerRect.width / 2;
  let left = triggerCenter - tooltipRect.width / 2;
  let maxLeft = innerWidth - tooltipRect.width - 2;

  {
    left: Js.Math.min_int(maxLeft, Js.Math.max_int(left, 2)) + scrollX,
    top: triggerRect.bottom + 8 + scrollY,
  };
};

[@bs.module "react"]
external cloneElement: (React.element, 'a) => React.element = "cloneElement";

[@react.component]
let make = (~label, ~children, ~maxWidth=250, ~canBeShowed=true) => {
  let (trigger, tooltip) = useTooltip();

  let {isVisible, triggerRect} = tooltip;

  <>
    {cloneElement(children, trigger)}
    {isVisible && canBeShowed
       ? <ReachUi.Portal>
           <div
             className={Cn.make([
               "absolute w-0 h-0 border-gray-800",
               Css.(
                 style([
                   unsafe("borderLeft", "10px solid transparent !important"),
                   unsafe("borderRight", "10px solid transparent !important"),
                   unsafe("borderBottom", "10px solid"),
                   left(
                     triggerRect
                     ->Option.mapWithDefault(0, triggerRect => {
                         triggerRect.left - 10 + triggerRect.width / 2
                       })
                     ->px,
                   ),
                   top(
                     triggerRect
                     ->Option.mapWithDefault(0, triggerRect => {
                         triggerRect.bottom + scrollY
                       })
                     ->px,
                   ),
                 ])
               ),
             ])}
           />
         </ReachUi.Portal>
       : React.null}
    {canBeShowed
       ? <Toolkit__Ui_Spread props=tooltip>
           <TooltipPopup
             label
             className={Cn.make([
               "px-3 py-2 bg-gray-800 rounded text-white text-sm z-30 whitespace-pre-wrap",
               Css.style([Css.maxWidth(maxWidth->Css.px)]),
             ])}
             position=centered
           />
         </Toolkit__Ui_Spread>
       : React.null}
  </>;
};
