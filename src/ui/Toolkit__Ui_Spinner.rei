type size = [ | `xs | `sm | `md | `lg | `xl];

type color = [
  | `current
  | `black
  | `white
  | `gray
  | `success
  | `primary
  | `info
  | `danger
  | `warning
];

[@react.component]
let make:
  (
    ~size: size=?, // default: `md
    ~color: color=?, // default: `current
    ~label: string=?, // default: "Loading..."
    ~className: string=?,
    ~style: ReactDOM.style=?
  ) =>
  React.element;
