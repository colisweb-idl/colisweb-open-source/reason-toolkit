type color = [
  | `black
  | `white
  | `gray
  | `primary
  | `info
  | `danger
  | `warning
  | `success
  | `neutral
];

type size = [ | `sm | `md | `lg | `xs];
type variant = [ | `plain | `outline];

[@react.component]
let make:
  (
    ~color: color=?,
    ~size: size=?,
    ~className: string=?,
    ~variant: variant=?,
    ~children: React.element
  ) =>
  React.element;
