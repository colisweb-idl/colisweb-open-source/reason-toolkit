[@react.component]
let make:
  (
    ~htmlFor: string,
    ~className: string=?,
    ~optionalMessage: React.element=?,
    ~children: React.element
  ) =>
  React.element;
