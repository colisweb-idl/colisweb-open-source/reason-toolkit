// ----------------------
// This component require the presence of a Toolkit Ui Snackbar provider for onCopy notifications to work
// ----------------------

[@react.component]
let make:
  (
    ~reference: string,
    ~onCopyNotificationMessage: string=?,
    ~className: string=?
  ) =>
  React.element;
