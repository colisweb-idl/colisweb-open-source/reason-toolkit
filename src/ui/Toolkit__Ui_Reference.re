// ----------------------
// This component require the presence of a Toolkit Ui Snackbar provider for onCopy notifications to work
// ----------------------

[@react.component]
let make =
    (~reference, ~onCopyNotificationMessage: option(string)=?, ~className="") => {
  let refClipboard =
    Toolkit__Hooks.useClipboard(reference, ~onCopyNotificationMessage?);

  <div
    className={Cn.make([
      "flex flex-row items-center bg-info-50 text-info-700 min-w-0 w-full",
      className,
    ])}>
    <p className="font-mono w-full text-sm pl-2 truncate">
      reference->React.string
    </p>
    <Toolkit__Ui_IconButton
      size=`xs
      color=`info
      onClick={_ => refClipboard.copy()}
      ariaLabel="copy"
      icon={<BsReactIcons.MdContentCopy size=9 />}
      className="flex-shrink-0"
    />
  </div>;
};
