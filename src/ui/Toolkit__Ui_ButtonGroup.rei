[@react.component]
let make: (~children: React.element, ~className: string=?, ~size: [ | `md | `xl]=?) => React.element