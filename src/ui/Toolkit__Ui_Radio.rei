[@react.component]
let make:
  (
    ~value: string,
    ~children: React.element,
    ~disabled: bool=?,
    ~onChange: ReactEvent.Form.t => unit=?,
    ~name: string=?,
    ~checked: bool=?,
    ~className: string=?
  ) =>
  React.element;
