type variant = [ | `default | `code];

let autoExpand: Dom.element => unit = [%bs.raw
  {|
  function (field) {
    field.style.height = 'inherit';

    var computed = window.getComputedStyle(field);
    var height = parseInt(computed.getPropertyValue('border-top-width'), 10)
                + parseInt(computed.getPropertyValue('padding-top'), 10)
                + field.scrollHeight
                + parseInt(computed.getPropertyValue('padding-bottom'), 10)
                + parseInt(computed.getPropertyValue('border-bottom-width'), 10);

    field.style.height = height + 'px';
  }
  |}
];

[@react.component]
let make =
  React.forwardRef(
    (
      ~id,
      ~name=?,
      ~value=?,
      ~variant: variant=`default,
      ~defaultValue=?,
      ~placeholder=?,
      ~autoFocus=?,
      ~disabled=?,
      ~required=?,
      ~onBlur: option(ReactEvent.Focus.t => unit)=?,
      ~onKeyDown: option(ReactEvent.Keyboard.t => unit)=?,
      ~onChange: option(ReactEvent.Form.t => unit)=?,
      ~isInvalid=?,
      ~className="",
      ref_,
    ) => {
    let ownRef = React.useRef(Js.Nullable.null);
    let textareaRef =
      switch (ref_->Js.Nullable.toOption) {
      | Some(ref_) => ref_
      | _ => ownRef
      };

    React.useLayoutEffect1(
      () => {
        if (textareaRef.current->Js.Nullable.toOption->Option.isSome) {
          textareaRef.current
          ->Js.Nullable.toOption
          ->Option.map(autoExpand)
          ->ignore;
        };
        None;
      },
      [|textareaRef|],
    );

    let onChange = event => {
      if (textareaRef.current->Js.Nullable.toOption->Option.isSome) {
        Js.Global.setTimeout(
          () => {
            textareaRef.current
            ->Js.Nullable.toOption
            ->Option.map(autoExpand)
            ->ignore
          },
          0,
        )
        ->ignore;
      };
      onChange->Option.map(onChange => onChange(event))->ignore;
    };

    <textarea
      ref={ReactDOMRe.Ref.callbackDomRef(ref_ => textareaRef.current = ref_)}
      style={ReactDOMRe.Style.make(
        ~height="auto",
        ~minHeight="38px",
        ~maxHeight="40vh",
        ~resize="none",
        (),
      )}
      className={Cn.make([
        className,
        "appearance-none outline-none transition-all duration-150 ease-in-out block w-full bg-white text-gray-800 border rounded py-2 px-4 leading-normal focus:z30 relative disabled:bg-gray-200 disabled:text-gray-700",
        variant == `code ? "font-code" : "font-sans",
        "border-danger-500 shadow-danger-500"
        ->Cn.ifTrue(isInvalid->Option.getWithDefault(false)),
      ])}
      spellCheck={variant == `code ? false : true}
      autoComplete={variant == `code ? "off" : "on"}
      id
      rows=1
      onChange
      ?name
      ?value
      ?defaultValue
      ?disabled
      ?required
      ?placeholder
      ?autoFocus
      ?onBlur
      ?onKeyDown
    />;
  });
