[@react.component]
let make = (~children, ~className="", ~size: [ | `md | `xl]=`md) => {
  let isMd = size === `md;
  <div
    className={Cn.make([
      className,
      "flex justify-center items-center rounded-full font-display bg-gray-200",
      isMd ? "h-6" : "h-10",
      isMd ? "text-sm" : "text-xl",
      Css.(
        style([
          selector(
            "& > button",
            [
              whiteSpace(`nowrap),
              height(100.->pct),
              padding2(
                ~v=0.75->rem,
                ~h={
                  isMd ? 0.75->rem : 1.->rem;
                },
              ),
              display(`flex),
              alignItems(`center),
              justifyContent(`center),
              borderRadius(2.->rem),
              selector("&:hover", [backgroundColor("3DDEF3"->hex)]),
            ],
          ),
          selector(
            "& > button.selected",
            [
              background(
                linearGradient(
                  270.->deg,
                  [(0.->pct, "11a3b6"->hex), (100.->pct, "27d0dc"->hex)],
                ),
              ),
              color(white),
            ],
          ),
        ])
      ),
    ])}>
    children
  </div>;
};
