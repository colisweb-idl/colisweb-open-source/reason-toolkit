[@react.component]
let make:
  (
    ~options: array((React.element, string, bool)),
    ~onChange: 'value => unit,
    ~placeholder: string=?,
    ~defaultValue: string=?,
    ~isDisabled: bool=?,
    ~isInvalid: bool=?,
    ~className: string=?,
    ~id: string=?,
    ~value: string=?
  ) =>
  React.element;
