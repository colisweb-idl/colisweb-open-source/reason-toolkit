type size = [ | `md | `lg];

let getBulletSize = size =>
  switch (size) {
  | `md => "w-5 h-5"
  | `lg => "w-6 h-6"
  };

let getLabelSize = size =>
  switch (size) {
  | `md => "w-12"
  | `lg => "w-16"
  };

let getTranslateBySize = size =>
  switch (size) {
  | `md => "translate-x-5"
  | `lg => "translate-x-8"
  };

[@react.component]
let make = (~onChange=?, ~name=?, ~checked=?, ~size=`md) => {
  let (isChecked, setIsChecked) =
    React.useState(() => checked->Option.getWithDefault(false));

  <label
    className={Cn.make([
      "flex items-center relative rounded-full py-1 cursor-pointer border transition-all",
      getLabelSize(size),
      isChecked ? "bg-info-500" : "bg-gray-300",
    ])}>
    <ReachUi.VisuallyHidden>
      <input
        type_="checkbox"
        className="mr-3 h-6"
        onChange={_ => {
          setIsChecked(v => !v);
          onChange->Option.map(fn => fn(!isChecked))->ignore;
        }}
        ?name
        ?checked
      />
    </ReachUi.VisuallyHidden>
    <span
      className={Cn.make([
        "rounded-full shadow transition duration-200 ease-linear transform bg-white",
        isChecked ? getTranslateBySize(size) : "translate-x-1",
        getBulletSize(size),
      ])}
    />
  </label>;
};
