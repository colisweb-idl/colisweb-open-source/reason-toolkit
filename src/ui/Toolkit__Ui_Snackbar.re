type state = {
  isVisible: bool,
  options: option(options),
}
and options = {
  title: string,
  description: option(React.element),
  closable: bool,
  variant,
  timeout: int,
}
and variant = [ | `success | `warning | `danger];

type action =
  | Show(options)
  | Hide;

let store =
  Restorative.createStore({isVisible: false, options: None}, (state, action) =>
    switch (action) {
    | Show(options) => {isVisible: true, options: Some(options)}
    | Hide => {...state, isVisible: false}
    }
  );

let show =
    (~title, ~description=?, ~closable=true, ~variant, ~timeout=5000, ()) =>
  store.dispatch(Show({title, description, closable, variant, timeout}));
let hide = () => store.dispatch(Hide);

module Provider = {
  [@react.component]
  let make = () => {
    let {isVisible, options} = store.useStore();
    let timeoutRef = React.useRef(None);

    React.useEffect1(
      () => {
        if (isVisible) {
          timeoutRef.current
          ->Option.map(timeoutRef => timeoutRef->Js.Global.clearTimeout)
          ->ignore;

          let id =
            Js.Global.setTimeout(
              () => hide(),
              options->Option.mapWithDefault(5000, option => option.timeout),
            );

          timeoutRef.current = Some(id);
        };
        None;
      },
      [|isVisible|],
    );

    <ReachUi.Portal>
      <div
        className={Cn.make([
          "absolute bg-white p-3 rounded shadow transition duration-500 ease-in-out right-0 mr-6 transform",
          Css.(style([bottom(40->px)])),
          isVisible
            ? "opacity-100 translate-y-0 z-50" : "opacity-0 translate-y-10",
          Cn.mapSome(options, options =>
            switch (options.variant) {
            | `success => "bg-success-500 text-white"
            | `warning => "bg-warning-500 text-white"
            | `danger => "bg-danger-500 text-white"
            }
          ),
        ])}>
        {options->Option.mapWithDefault(React.null, options => {
           <div className="flex flex-gap-3">
             <div>
               {switch (options.variant) {
                | `success => <BsReactIcons.MdCheckCircle size=28 />
                | `warning => <BsReactIcons.MdWarning size=28 />
                | `danger => <BsReactIcons.MdError size=28 />
                }}
             </div>
             <div>
               <strong> options.title->React.string </strong>
               {options.description
                ->Option.mapWithDefault(React.null, description =>
                    <p
                      className={Cn.make([
                        "text-sm",
                        Css.(style([maxWidth(250->px)])),
                      ])}>
                      description
                    </p>
                  )}
             </div>
             {options.closable
                ? <button className="self-start" onClick={_ => hide()}>
                    <BsReactIcons.MdClose />
                  </button>
                : React.null}
           </div>
         })}
      </div>
    </ReachUi.Portal>;
  };
};
