[@react.component]
let make = (~props, ~children) =>
  ReasonReact.cloneElement(children, ~props=props->Obj.magic, [||]);
