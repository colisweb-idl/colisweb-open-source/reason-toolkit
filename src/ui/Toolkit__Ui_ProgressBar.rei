type color = [ | `success | `danger | `warning | `info];

[@react.component]
let make: (~progression: float, ~color: color=?) => React.element;
