module Header: {
  type size = [ | `sm | `md];
  type action = {
    label: React.element,
    event: unit => unit,
  };
  [@react.component]
  let make:
    (
      ~className: string=?,
      ~children: React.element=?,
      ~action: action=?,
      ~actionDisabled: bool=?
    ) =>
    React.element;
};

module Body: {
  [@react.component]
  let make:
    (~className: string=?, ~children: React.element=?) => React.element;
};

module Message: {
  type variant = [ | `info | `success | `warning | `error];

  [@react.component]
  let make:
    (~variant: variant, ~className: string=?, ~children: React.element=?) =>
    React.element;
};

module Footer: {
  [@react.component]
  let make:
    (~className: string=?, ~children: React.element=?) => React.element;
};

module FixedBody: {
  [@react.component]
  let make:
    (~className: string=?, ~children: React.element=?) =>
    React.element;
};

[@react.component]
let make:
  (
    ~className: string=?,
    ~children: React.element=?,
    ~customMinHeight: int=?
  ) =>
  React.element;
