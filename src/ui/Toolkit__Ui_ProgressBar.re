type color = [ | `success | `danger | `warning | `info];

[@react.component]
let make = (~progression: float, ~color: color=`success) => {
  <div className="flex flex-row w-full h-2 bg-neutral-300 rounded-lg">
    <div
      className={Cn.make([
        "h-full rounded-lg",
        switch (color) {
        | `success => "bg-success-500"
        | `danger => "bg-danger-500"
        | `warning => "bg-warning-500"
        | `info => "bg-info-500"
        },
        Css.style([Css.width(progression->`percent)]),
      ])}
    />
  </div>;
};
