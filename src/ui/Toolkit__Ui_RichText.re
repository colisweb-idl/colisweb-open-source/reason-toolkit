let transformUrl = text => {
  text
  |> Js.String.unsafeReplaceBy0(
       [%re "/(https?:\\/\\/[^\\s]+)/g"], (url, _, _) =>
       {j|<a target='_blank' class='text-blue-500 underline' href="$url">$url</a>|j}
     );
};

[@react.component]
let make = (~text) => {
  <span dangerouslySetInnerHTML={"__html": text->transformUrl} />;
};
