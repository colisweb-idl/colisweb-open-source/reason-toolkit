module Spinner = Toolkit__Ui_Spinner;

type variant = [ | `default | `text | `link | `outline | `pill];

type size = [ | `xs | `sm | `md | `lg];

type color = [
  | `black
  | `white
  | `gray
  | `success
  | `info
  | `warning
  | `primary
  | `danger
  | `neutral
  | `neutralLight
];

let baseStyle = () => "font-sans font-semibold rounded inline-flex items-center justify-center transition duration-150 ease-in-out relative focus:z30";

let colorStyle = (~color: color, ~variant, ~disabled) => {
  switch (variant) {
  | `pill
  | `default =>
    switch (color) {
    | `black => "bg-gray-900 hover:bg-gray-800 disabled:bg-gray-800 text-white border border-transparent"
    | `white => "bg-white hover:bg-gray-100 text-gray-800 border border-gray-300"
    | `gray => "bg-gray-200 hover:bg-gray-300 disabled:bg-gray-300 text-gray-800 border border-gray-300"
    | `success => "bg-success-600 hover:bg-success-700 disabled:bg-success-700 text-white border border-transparent"
    | `info => "bg-info-50 hover:bg-info-600 text-info-500 disabled:hover:bg-info-50 disabled:hover:text-info-500 hover:text-white border border-transparent"
    | `warning => "bg-warning-600 hover:bg-warning-700 disabled:bg-warning-600 text-white border border-transparent"
    | `primary => "bg-primary-700 hover:bg-primary-600 disabled:bg-primary-600 text-white border border-transparent"
    | `danger => "bg-danger-500 hover:bg-danger-600 disabled:bg-danger-500 text-white border border-danger-500"
    | `neutral =>
      Cn.make([
        "bg-neutral-700 text-white border border-transparent font-display",
        disabled ? "disabled:bg-neutral-800" : "hover:bg-neutral-800",
      ])

    | `neutralLight =>
      Cn.make([
        "bg-neutral-200 border border-neutral-200 text-primary-700 font-display",
        disabled
          ? "disabled:bg-neutral-100"
          : "hover:bg-primary-800 hover:border-primary-200 hover:text-white",
      ])
    }
  | `text =>
    switch (color) {
    | `black
    | `white
    | `gray => "uppercase text-gray-800 hover:bg-gray-100 disabled:bg-gray-200"
    | `success => "uppercase text-success-600 hover:bg-success-50 disabled:bg-success-50"
    | `info => "uppercase text-info-600 hover:bg-info-50 disabled:bg-info-50"
    | `warning => "uppercase text-warning-600 hover:bg-warning-50"
    | `primary => "uppercase text-primary-500 hover:bg-primary-50"
    | `danger => "uppercase hover:bg-danger-50 text-danger-600"
    | `neutral => "hover:bg-neutral-100 text-neutral-700 hover:border-neutral-400 disabled:text-neutral-500 uppercase"
    | `neutralLight => "uppercase text-primary-700 hover:bg-neutral-100 disabled:text-neutral-400"
    }

  | `link => "text-info-500 hover:underline"
  | `outline =>
    switch (color) {
    | `black
    | `white
    | `gray => "border border-gray-100 text-gray-800 hover:bg-gray-100 disabled:bg-gray-200"
    | `success => "border border-success-600 text-success-600 hover:bg-success-50 disabled:bg-success-50"
    | `info => "border border-info-600 text-info-600 hover:bg-info-50 disabled:bg-info-50"
    | `warning => "border border-warning-600 text-warning-600 hover:bg-warning-50 disabled:bg-warning-50"
    | `neutral => "border border-neutral-700 text-neutral-700 hover:bg-neutral-200 disabled:bg-neutral-100"
    | `neutralLight => "border border-neutral-300 text-primary-700 hover:bg-neutral-200 disabled:bg-neutral-100"
    | `danger => "border border-danger-400 text-danger-600 hover:bg-danger-50 disabled:bg-danger-300"
    | `primary => "border border-primary-500 text-primary-500 hover:bg-primary-50"
    }
  };
};

let buttonStyle =
    (
      ~size: size=`md,
      ~variant: variant=`default,
      ~color: color=`white,
      ~disabled,
      (),
    )
    : string => {
  let sizeStyle =
    switch (size) {
    | `xs => "text-sm normal-case px-2"
    | `sm => "text-sm leading-4 h-8 px-2"
    | `md => "text-base leading-5 py-2 px-4"
    | `lg => "text-lg leading-8 py-2 px-6"
    };

  let colorStyle = colorStyle(~color, ~variant, ~disabled);

  let baseStyle = baseStyle();

  let disabledStyle =
    disabled ? "opacity-50 cursor-not-allowed shadow-none" : "";

  Cn.make([baseStyle, sizeStyle, colorStyle, disabledStyle]);
};

[@react.component]
let make =
  React.forwardRef(
    (
      ~size=`md,
      ~color=`white,
      ~variant=`default,
      ~isLoading=false,
      ~onClick: option(ReactEvent.Mouse.t => unit)=?,
      ~onBlur: option(ReactEvent.Focus.t => unit)=?,
      ~onFocus: option(ReactEvent.Focus.t => unit)=?,
      ~disabled=false,
      ~tabIndex: option(int)=?,
      ~ariaLabel: option(string)=?,
      ~ariaLabelledby: option(string)=?,
      ~ariaExpanded: option(bool)=?,
      ~autoFocus: option(bool)=?,
      ~id: option(string)=?,
      ~type_: option(string)=?,
      ~className="",
      ~children=React.null,
      ~leftIcon=?,
      ~rightIcon=?,
      ref_,
    ) => {
    <button
      ref=?{ref_->Js.Nullable.toOption->Option.map(ReactDOMRe.Ref.domRef)}
      ?ariaLabel
      ?ariaLabelledby
      ?ariaExpanded
      ?autoFocus
      disabled={isLoading || disabled}
      ?type_
      ?id
      ?tabIndex
      ?onFocus
      ?onBlur
      ?onClick
      className={Cn.make([
        className,
        buttonStyle(~color, ~variant, ~size, ~disabled, ()),
        "rounded-full"->Cn.ifTrue(variant === `pill),
      ])}>
      {leftIcon->Option.mapWithDefault(React.null, icon => {
         <span className="mr-2"> icon </span>
       })}
      children
      {rightIcon->Option.mapWithDefault(React.null, icon => {
         <span className="ml-2"> icon </span>
       })}
      {isLoading
         ? <Spinner size=`sm color=`current className="ml-2" /> : React.null}
    </button>
  });
