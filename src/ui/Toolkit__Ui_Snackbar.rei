type variant = [ | `success | `warning | `danger];

let show:
  (
    ~title: string,
    ~description: React.element=?, // default: None
    ~closable: bool=?, // default: true
    ~variant: variant,
    ~timeout: int=?, // default: 5000
    unit
  ) =>
  unit;
let hide: unit => unit;

module Provider: {
  [@react.component]
  let make: unit => React.element;
};
