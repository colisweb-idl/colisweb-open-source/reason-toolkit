[@react.component]
let make =
    (
      ~value: string,
      ~children: React.element,
      ~disabled: option(bool)=?,
      ~onChange: option(ReactEvent.Form.t => unit)=?,
      ~name: option(string)=?,
      ~checked: option(bool)=?,
      ~className: string="",
    ) =>
  <label
    className={Cn.make([
      "flex items-center",
      disabled->Option.getWithDefault(false)
        ? "cursor-not-allowed opacity-75 text-gray-600" : "cursor-pointer",
      className,
    ])}>
    <input
      type_="radio"
      value
      className="mr-3 h-6"
      ?onChange
      ?disabled
      ?name
      ?checked
    />
    children
  </label>;
