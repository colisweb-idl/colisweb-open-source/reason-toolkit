type size = [ | `xs | `sm | `md | `lg];

module Styles = {
  open Css;

  let label =
    style([
      selector("& > input + .checkmark", [backgroundColor(hex("fff"))]),
      selector(
        "& > input:checked + .checkmark",
        [backgroundColor(hex("15cbe3")), borderColor(hex("15cbe3"))],
      ),
      selector("& > input + .checkmark > *", [opacity(0.)]),
      selector("& > input:checked + .checkmark > *", [opacity(1.)]),
    ]);
};

[@react.component]
let make =
    (
      ~value,
      ~children: option(React.element)=?,
      ~disabled: option(bool)=?,
      ~onChange=?,
      ~name=?,
      ~checked=?,
      ~className="",
      ~size: size=`sm,
    ) =>
  <label
    className={Cn.make([
      Styles.label,
      "flex items-center",
      disabled->Option.getWithDefault(false)
        ? "cursor-not-allowed opacity-75 text-gray-600" : "cursor-pointer",
      className,
    ])}>
    <input
      type_="checkbox"
      value
      className="hidden"
      onChange={event => {
        let target = ReactEvent.Form.target(event);
        let checked = target##checked;
        let value = target##value;

        onChange->Option.forEach(fn => fn(checked, value));
      }}
      ?disabled
      ?name
      ?checked
    />
    <span
      className={Cn.make([
        "checkmark rounded border border-neutral-300 text-white mr-3 transform transition-all ease-in-out flex items-center justify-center",
        switch (size) {
        | `xs => "w-4 h-4"
        | `sm => "w-6 h-6"
        | `md => "w-8 h-8"
        | `lg => "w-10 h-10"
        },
      ])}>
      <BsReactIcons.FaCheck
        className="transform transition-all ease-in-out"
        size={
          switch (size) {
          | `xs => 14
          | `sm => 16
          | `md => 18
          | `lg => 24
          }
        }
      />
    </span>
    {children->Option.getWithDefault(React.null)}
  </label>;
