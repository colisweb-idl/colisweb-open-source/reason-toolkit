include ReachUi_Utils;

module Menu = {
  type renderProps = {isOpen: bool};

  [@bs.module "@reach/menu-button"] [@react.component]
  external make: (~children: renderProps => React.element) => React.element =
    "Menu";
};

module MenuButton = {
  [@bs.module "@reach/menu-button"] [@react.component]
  external make:
    (
      ~onKeyDown: ReactEvent.Keyboard.t => unit=?,
      ~onMouseDown: ReactEvent.Mouse.t => unit=?,
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element
    ) =>
    React.element =
    "MenuButton";
};

module MenuList = {
  [@bs.module "@reach/menu-button"] [@react.component]
  external make:
    (
      ~portal: bool=?,
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element
    ) =>
    React.element =
    "MenuList";
};

module MenuPopover = {
  [@bs.module "@reach/menu-button"] [@react.component]
  external make:
    (
      ~portal: bool=?,
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element
    ) =>
    React.element =
    "MenuPopover";
};

module MenuItems = {
  [@bs.module "@reach/menu-button"] [@react.component]
  external make:
    (
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element
    ) =>
    React.element =
    "MenuItems";
};

module MenuItem = {
  type props = {
    .
    "onSelect": unit => unit,
    "onMouseUp": option(ReactEvent.Mouse.t => unit),
    "onKeyDown": option(ReactEvent.Keyboard.t => unit),
    "as": option(as_),
    "style": option(ReactDOMRe.Style.t),
    "className": option(string),
    "children": React.element,
  };

  [@bs.module "@reach/menu-button"]
  external make: React.component(props) = "MenuItem";

  let makeProps =
      (
        ~onSelect: unit => unit,
        ~onMouseUp: option(ReactEvent.Mouse.t => unit)=?,
        ~onKeyDown: option(ReactEvent.Keyboard.t => unit)=?,
        ~as_: option(as_)=?,
        ~style: option(ReactDOMRe.Style.t)=?,
        ~className: option(string)=?,
        ~children: React.element,
        (),
      ) => {
    "onSelect": onSelect,
    "onMouseUp": onMouseUp,
    "onKeyDown": onKeyDown,
    "as": as_,
    "style": style,
    "className": className,
    "children": children,
  };
};

module MenuLink = {
  type props = {
    .
    "onSelect": option(unit => unit),
    "onMouseUp": option(ReactEvent.Mouse.t => unit),
    "onKeyDown": option(ReactEvent.Keyboard.t => unit),
    "as": option(as_),
    "href": option(string),
    "style": option(ReactDOMRe.Style.t),
    "className": option(string),
    "children": React.element,
  };

  [@bs.module "@reach/menu-button"]
  external make: React.component(props) = "MenuLink";

  let makeProps =
      (
        ~onSelect: option(unit => unit)=?,
        ~onMouseUp: option(ReactEvent.Mouse.t => unit)=?,
        ~onKeyDown: option(ReactEvent.Keyboard.t => unit)=?,
        ~as_: option(as_)=?,
        ~href: option(string)=?,
        ~style: option(ReactDOMRe.Style.t)=?,
        ~className: option(string)=?,
        ~children: React.element,
        (),
      ) => {
    "onSelect": onSelect,
    "as": as_,
    "onMouseUp": onMouseUp,
    "onKeyDown": onKeyDown,
    "href": href,
    "style": style,
    "className": className,
    "children": children,
  };
};