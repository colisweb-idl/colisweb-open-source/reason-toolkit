module AlertDialog = {
  type props('a) = {
    .
    "isOpen": bool,
    "onDismiss": unit => unit,
    "leastDestructiveRef": React.ref('a),
    "aria-label": option(string),
    "aria-labelledby": option(string),
    // "allowPinchZoom": option(bool),
    "style": option(ReactDOMRe.Style.t),
    "className": option(string),
    "children": React.element,
  };

  [@bs.module "@reach/alert-dialog"]
  external make: React.component(props('a)) = "AlertDialog";

  let makeProps =
      (
        ~isOpen: bool,
        ~onDismiss: unit => unit,
        ~leastDestructiveRef: React.ref('a),
        ~ariaLabel: option(string)=?,
        ~ariaLabelledby: option(string)=?,
        // Disable allowPinchZoom for now since it causes an annoying react dom warning
        // ~allowPinchZoom: option(bool)=?,
        ~style: option(ReactDOMRe.Style.t)=?,
        ~className: option(string)=?,
        ~children: React.element,
        (),
      ) => {
    "isOpen": isOpen,
    "onDismiss": onDismiss,
    "leastDestructiveRef": leastDestructiveRef,
    "aria-label": ariaLabel,
    "aria-labelledby": ariaLabelledby,
    "style": style,
    "className": className,
    // "allowPinchZoom": allowPinchZoom,
    "children": children,
  };
};

module AlertDialogLabel = {
  [@bs.module "@reach/alert-dialog"] [@react.component]
  external make:
    (
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element
    ) =>
    React.element =
    "AlertDialogLabel";
};

module AlertDialogDescription = {
  [@bs.module "@reach/alert-dialog"] [@react.component]
  external make:
    (
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element
    ) =>
    React.element =
    "AlertDialogDescription";
};

module AlertDialogContent = {
  type props = {
    .
    "aria-label": option(string),
    "aria-labelledby": option(string),
    "style": option(ReactDOMRe.Style.t),
    "className": option(string),
    "children": React.element,
  };

  [@bs.module "@reach/alert-dialog"]
  external make: React.component(props) = "AlertDialogContent";

  let makeProps =
      (
        ~ariaLabel: option(string)=?,
        ~ariaLabelledby: option(string)=?,
        ~style: option(ReactDOMRe.Style.t)=?,
        ~className: option(string)=?,
        ~children: React.element,
        (),
      ) => {
    "aria-label": ariaLabel,
    "aria-labelledby": ariaLabelledby,
    "style": style,
    "className": className,
    "children": children,
  };
};

module AlertDialogOverlay = {
  [@bs.module "@reach/alert-dialog"] [@react.component]
  external make:
    (
      ~initialFocusRef: React.ref('a)=?,
      ~leastDestructiveRef: React.ref('a)=?,
      ~allowPinchZoom: bool=?,
      ~isOpen: bool,
      ~onDismiss: unit => unit,
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element
    ) =>
    React.element =
    "AlertDialogOverlay";
};
