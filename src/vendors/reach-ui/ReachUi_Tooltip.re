type position = {
  top: int,
  left: int,
};

type tooltip = {
  triggerRect: option(triggerRect),
  isVisible: bool,
}
and triggerRect = {
  left: int,
  bottom: int,
  top: int,
  right: int,
  width: int,
};

module Tooltip = {
  [@bs.module "@reach/tooltip"] [@react.component]
  external make:
    (~className: string=?, ~children: React.element, ~label: React.element) =>
    React.element =
    "default";
};
module TooltipPopup = {
  [@bs.module "@reach/tooltip"] [@react.component]
  external make:
    (
      ~className: string=?,
      ~position: (triggerRect, triggerRect) => position,
      ~label: React.element
    ) =>
    React.element =
    "TooltipPopup";
};

[@bs.module "@reach/tooltip"]
external useTooltip: unit => (unit => unit, tooltip) = "useTooltip";
