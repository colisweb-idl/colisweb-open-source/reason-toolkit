[@unboxed]
type as_ =
  | As('a): as_;
let dom = (v: string) => As(v);
let component = (v: React.componentLike('props, React.element)) => As(v);