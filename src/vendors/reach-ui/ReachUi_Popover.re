[@bs.module "@reach/popover"] [@react.component]
external make:
  (
    ~targetRef: React.ref('a),
    ~position: 'position=?,
    ~style: ReactDOMRe.Style.t=?,
    ~className: string=?,
    ~children: React.element
  ) =>
  React.element =
  "default";

[@bs.module "@reach/popover"] [@react.component]
external positionDefault: 'position = "positionDefault";

[@bs.module "@reach/popover"] [@react.component]
external positionMatchWidth: 'position = "positionMatchWidth";
