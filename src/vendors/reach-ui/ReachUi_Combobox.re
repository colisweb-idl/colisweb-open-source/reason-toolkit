include ReachUi_Utils;

module Listbox = {
  [@bs.module "@reach/listbox"] [@react.component]
  external make:
    (
      ~arrow: React.element=?,
      ~button: React.element=?,
      ~defaultValue: string=?,
      ~value: string=?,
      ~onChange: string => unit=?,
      ~disabled: bool=?,
      ~required: bool=?,
      ~form: string=?,
      ~name: string=?,
      ~portal: bool=?,
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element
    ) =>
    React.element =
    "Listbox";
};

module ListboxInput = {
  type renderProps = {
    value: Js.Nullable.t(string),
    valueLabel: Js.Nullable.t(string),
    expanded: bool,
  };

  type props = {
    .
    "aria-label": option(string),
    "aria-labelledby": option(string),
    "defaultValue": option(string),
    "value": option(string),
    "onChange": option(string => unit),
    "disabled": option(bool),
    "required": option(bool),
    "form": option(string),
    "name": option(string),
    "style": option(ReactDOMRe.Style.t),
    "className": option(string),
    "children": renderProps => React.element,
  };

  [@bs.module "@reach/listbox"]
  external make: React.component(props) = "ListboxInput";

  let makeProps =
      (
        ~ariaLabel: option(string)=?,
        ~ariaLabelledby: option(string)=?,
        ~defaultValue: option(string)=?,
        ~value: option(string)=?,
        ~onChange: option(string => unit)=?,
        ~disabled: option(bool)=?,
        ~required: option(bool)=?,
        ~form: option(string)=?,
        ~name: option(string)=?,
        ~style: option(ReactDOMRe.Style.t)=?,
        ~className: option(string)=?,
        ~children: renderProps => React.element,
        (),
      ) => {
    "aria-label": ariaLabel,
    "aria-labelledby": ariaLabelledby,
    "defaultValue": defaultValue,
    "value": value,
    "onChange": onChange,
    "disabled": disabled,
    "required": required,
    "form": form,
    "name": name,
    "style": style,
    "className": className,
    "children": children,
  };
};

module ListboxButton = {
  type props('a) = {
    .
    "aria-label": option(string),
    "arrow": option(bool),
    "as": option(as_),
    "ref": option(React.ref('a)),
    "onKeyDown": option(ReactEvent.Keyboard.t => unit),
    "onMouseUp": option(ReactEvent.Mouse.t => unit),
    "style": option(ReactDOMRe.Style.t),
    "className": option(string),
    "children": option(React.element),
  };

  [@bs.module "@reach/listbox"]
  external make: React.component(props('a)) = "ListboxButton";

  let makeProps =
      (
        ~ariaLabel: option(string)=?,
        ~arrow: option(bool)=?,
        ~as_: option(as_)=?,
        ~ref: option(React.ref('a))=?,
        ~onKeyDown: option(ReactEvent.Keyboard.t => unit)=?,
        ~onMouseUp: option(ReactEvent.Mouse.t => unit)=?,
        ~style: option(ReactDOMRe.Style.t)=?,
        ~className: option(string)=?,
        ~children: option(React.element)=?,
        (),
      ) => {
    "aria-label": ariaLabel,
    "arrow": arrow,
    "as": as_,
    "ref": ref,
    "onKeyDown": onKeyDown,
    "onMouseUp": onMouseUp,
    "style": style,
    "className": className,
    "children": children,
  };
};

module ListboxArrow = {
  [@bs.module "@reach/listbox"] [@react.component]
  external make:
    (
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element=?
    ) =>
    React.element =
    "ListboxArrow";
};

module ListboxPopover = {
  [@bs.module "@reach/listbox"] [@react.component]
  external make:
    (
      ~portal: bool=?,
      ~ref: React.ref('a)=?,
      ~onKeyDown: ReactEvent.Keyboard.t => unit=?,
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element
    ) =>
    React.element =
    "ListboxPopover";
};

module ListboxList = {
  type props('a) = {
    .
    "as": option(as_),
    "ref": option(React.ref('a)),
    "style": option(ReactDOMRe.Style.t),
    "className": option(string),
    "children": React.element,
  };

  [@bs.module "@reach/listbox"]
  external make: React.component(props('a)) = "ListboxList";

  let makeProps =
      (
        ~as_: option(as_)=?,
        ~ref: option(React.ref('a))=?,
        ~style: option(ReactDOMRe.Style.t)=?,
        ~className: option(string)=?,
        ~children: React.element,
        (),
      ) => {
    "as": as_,
    "ref": ref,
    "style": style,
    "className": className,
    "children": children,
  };
};

module ListboxOption = {
  type props = {
    .
    "value": option(string),
    "label": option(string),
    "disabled": option(bool),
    "onMouseUp": option(ReactEvent.Mouse.t => unit),
    "onKeyDown": option(ReactEvent.Keyboard.t => unit),
    "as": option(as_),
    "style": option(ReactDOMRe.Style.t),
    "className": option(string),
    "children": React.element,
  };

  [@bs.module "@reach/listbox"]
  external make: React.component(props) = "ListboxOption";

  let makeProps =
      (
        ~value: option(string)=?,
        ~label: option(string)=?,
        ~disabled: option(bool)=?,
        ~onMouseUp: option(ReactEvent.Mouse.t => unit)=?,
        ~onKeyDown: option(ReactEvent.Keyboard.t => unit)=?,
        ~as_: option(as_)=?,
        ~style: option(ReactDOMRe.Style.t)=?,
        ~className: option(string)=?,
        ~children: React.element,
        (),
      ) => {
    "value": value,
    "label": label,
    "disabled": disabled,
    "onMouseUp": onMouseUp,
    "onKeyDown": onKeyDown,
    "as": as_,
    "style": style,
    "className": className,
    "children": children,
  };
};

module ListboxGroup = {
  [@bs.module "@reach/listbox"] [@react.component]
  external make:
    (
      ~label: string=?,
      ~style: ReactDOMRe.Style.t=?,
      ~className: string=?,
      ~children: React.element
    ) =>
    React.element =
    "ListboxGroup";
};

module ListboxGroupLabel = {
  type props = {
    .
    "as": option(as_),
    "style": option(ReactDOMRe.Style.t),
    "className": option(string),
    "children": React.element,
  };

  [@bs.module "@reach/listbox"]
  external make: React.component(props) = "ListboxGroupLabel";

  let makeProps =
      (
        ~as_: option(as_)=?,
        ~style: option(ReactDOMRe.Style.t)=?,
        ~className: option(string)=?,
        ~children: React.element,
        (),
      ) => {
    "as": as_,
    "style": style,
    "className": className,
    "children": children,
  };
};
