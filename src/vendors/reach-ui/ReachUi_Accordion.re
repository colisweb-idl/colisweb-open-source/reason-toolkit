include ReachUi_Utils;

module Accordion = {
  [@bs.module "@reach/accordion"] [@react.component]
  external make:
    (
      ~children: React.element,
      ~index: option(int)=?,
      ~defaultIndex: option(int)=?,
      ~collapsible: option(bool)=?,
      ~multiple: option(bool)=?,
      ~readOnly: option(bool)=?,
      ~onChange: option(int => unit)=?,
      ~readOnly: option(bool)=?
    ) =>
    React.element =
    "Accordion";
};

module AccordionItem = {
  [@bs.module "@reach/accordion"] [@react.component]
  external make:
    (~children: React.element, ~_as: option(as_)=?) => React.element =
    "AccordionItem";
};

module AccordionPanel = {
  [@bs.module "@reach/accordion"] [@react.component]
  external make:
    (~children: React.element, ~_as: option(as_)=?) => React.element =
    "AccordionPanel";
};
module AccordionButton = {
  [@bs.module "@reach/accordion"] [@react.component]
  external make:
    (~className: string=?, ~children: React.element, ~_as: option(as_)=?) =>
    React.element =
    "AccordionButton";
};

module ControlledAccordionMultiple = {
  [@bs.module "@reach/accordion"] [@react.component]
  external make:
    (
      ~children: React.element,
      ~index: option(array(int))=?,
      ~defaultIndex: option(int)=?,
      ~collapsible: option(bool)=?,
      ~multiple: option(bool)=?,
      ~readOnly: option(bool)=?,
      ~onChange: option(int => unit)=?,
      ~readOnly: option(bool)=?
    ) =>
    React.element =
    "Accordion";
};
