module Sentry = {
  module Scope = {
    type t;

    [@bs.send] external setExtra: (t, string, 'a) => unit = "setExtra";
  };

  [@bs.module "@sentry/react-native"]
  external init: Js.t('a) => unit = "init";

  [@bs.module "@sentry/react-native"]
  external setTag: (string, string) => unit = "setTag";

  [@bs.module "@sentry/react-native"]
  external captureException: Js.Promise.error => unit = "captureException";

  [@bs.module "@sentry/react-native"]
  external captureMessage: string => unit = "captureMessage";

  [@bs.module "@sentry/react-native"]
  external withScope: (Scope.t => unit) => unit = "withScope";
};