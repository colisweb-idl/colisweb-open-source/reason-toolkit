/**
 * Bindings of https://www.npmjs.com/package/analytics
 */

type plugin;

type options = {
  app: string,
  version: int,
  plugins: array(plugin),
};

type instance;
type gaOptions = {
  trackingId: string,
  anonymizeIp: bool,
  customDimensions: Js.Dict.t(string),
  setCustomDimensionsToPage: bool,
};

[@bs.module "analytics"] external init: options => instance = "default";

[@bs.send]
external identify: (instance, string, Js.Dict.t(string)) => unit = "identify";
[@bs.send]
external page: (instance, ~dimensions: Js.Dict.t(string)=?, unit) => unit =
  "page";

[@bs.send]
external track:
  (instance, string, ~payload: Js.Dict.t(string)=?, unit) => unit =
  "track";

/**
 * Bindings of https://www.npmjs.com/package/@analytics/google-analytics
 */
[@bs.module "@analytics/google-analytics"]
external googleAnalytics: gaOptions => plugin = "default";
