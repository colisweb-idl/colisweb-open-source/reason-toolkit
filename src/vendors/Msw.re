type mockedResponse;
external mockedResponse: 'a => Js.Null.t(mockedResponse) = "%identity";

[@bs.get] external body: mockedResponse => string = "body";

type responseTransformer = mockedResponse => mockedResponse;

type headers;
[@bs.send] external setHeader: (headers, string, 'value) => unit = "set";
[@bs.send] external getHeader: (headers, string) => 'value = "get";

type params;

type request = {
  url: Webapi.Url.t,
  method: string,
  headers,
  params,
};

type context;

type response;

[@bs.send]
external response:
  (response, Js.null('a), array(responseTransformer)) =>
  Js.Null.t(mockedResponse) =
  "apply";
let response = (res, responseTransformers) =>
  response(res, Js.null, responseTransformers);

type responseResolver =
  (request, response, context) => Js.Promise.t(Js.Null.t(mockedResponse));

[@bs.send]
external status: (context, string) => responseTransformer = "status";

[@bs.send] external json: (context, 'data) => responseTransformer = "json";

[@bs.send] external text: (context, 'data) => responseTransformer = "text";

[@bs.send] external xml: (context, 'data) => responseTransformer = "xml";

[@bs.send] external delay: (context, int) => responseTransformer = "delay";

type requestHandler;

type rest = {
  get: (string, responseResolver) => requestHandler,
  post: (string, responseResolver) => requestHandler,
  put: (string, responseResolver) => requestHandler,
  patch: (string, responseResolver) => requestHandler,
  delete: (string, responseResolver) => requestHandler,
  options: (string, responseResolver) => requestHandler,
};

[@bs.module "msw"] external rest: rest = "rest";

[@bs.module "msw"]
external restDict: Js.Dict.t((string, responseResolver) => requestHandler) =
  "rest";

type worker;

[@bs.module "msw"] [@bs.variadic]
external setupWorker: array(requestHandler) => worker = "setupWorker";

[@bs.send] external start: worker => unit = "start";

[@bs.send] external stop: worker => unit = "stop";

[@bs.send] [@bs.variadic]
external use: (worker, array(requestHandler)) => unit = "use";

[@bs.send] external restoreHandlers: worker => unit = "restoreHandlers";

[@bs.send] [@bs.variadic]
external resetHandlers: (worker, array(requestHandler)) => unit =
  "resetHandlers";

module Node = {
  type server;

  [@bs.module "msw/node"] [@bs.variadic]
  external setupServer: array(requestHandler) => server = "setupServer";

  [@bs.send] external listen: server => unit = "listen";

  [@bs.send] external close: server => unit = "close";

  [@bs.send] [@bs.variadic]
  external use: (server, array(requestHandler)) => unit = "use";

  [@bs.send] external restoreHandlers: server => unit = "restoreHandlers";

  [@bs.send] [@bs.variadic]
  external resetHandlers: (server, array(requestHandler)) => unit =
    "resetHandlers";
};
