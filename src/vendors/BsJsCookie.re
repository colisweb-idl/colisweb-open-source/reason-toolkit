module JsCookie = {
  type options = {expires: int};

  [@bs.module "js-cookie"]
  external set: (string, string, options) => unit = "set";

  [@bs.module "js-cookie"] external get: string => option(string) = "get";
};
