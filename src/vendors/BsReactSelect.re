/**
 * Bindings of https://www.npmjs.com/package/react-select
 */

type optionMessage = {inputValue: string};

type labelOption('value) = {
  label: string,
  value: 'value,
};

type optionMessageCb = {inputValue: string};

module ReactSelect = {
  [@bs.module "react-select"] [@react.component]
  external make:
    (
      ~name: string,
      ~autoFocus: option(bool)=?,
      ~isMulti: option(bool)=?,
      ~isDisabled: option(bool)=?,
      ~isLoading: option(bool)=?,
      ~isSearchable: option(bool)=?,
      ~placeholder: option(string)=?,
      ~className: option(string)=?,
      ~classNamePrefix: option(string)=?,
      ~value: option(labelOption('value))=?,
      ~defaultValue: option('value)=?,
      ~onChange: labelOption('value) => unit,
      ~options: array(labelOption('value)),
      ~noOptionsMessage: option(optionMessage => Js.Nullable.t(string))=?
    ) =>
    React.element =
    "default";
};

module ReactSelectMultiple = {
  module ReactSelect = {
    [@bs.module "react-select"] [@react.component]
    external make:
      (
        ~name: string,
        ~autoFocus: option(bool)=?,
        ~isMulti: option(bool)=?,
        ~isDisabled: option(bool)=?,
        ~isLoading: option(bool)=?,
        ~isSearchable: option(bool)=?,
        ~placeholder: option(string)=?,
        ~className: option(string)=?,
        ~classNamePrefix: option(string)=?,
        ~defaultValue: array(labelOption('value))=?,
        ~onChange: array(labelOption('value)) => unit,
        ~options: array(labelOption('value)),
        ~noOptionsMessage: option(optionMessage => Js.Nullable.t(string))=?
      ) =>
      React.element =
      "default";
  };

  [@react.component]
  let make =
      (
        ~name: string,
        ~autoFocus: option(bool)=?,
        ~isSearchable: option(bool)=?,
        ~isDisabled: option(bool)=?,
        ~isLoading: option(bool)=?,
        ~placeholder: option(string)=?,
        ~className: option(string)=?,
        ~classNamePrefix: option(string)=?,
        ~defaultValue: array(labelOption('value)),
        ~onChange: array(labelOption('value)) => unit,
        ~options: array(labelOption('value)),
        ~noOptionsMessage: option(optionMessage => Js.Nullable.t(string))=?,
      ) =>
    <ReactSelect
      name
      ?autoFocus
      ?isLoading
      ?isDisabled
      isMulti=true
      ?isSearchable
      ?placeholder
      ?className
      ?classNamePrefix
      defaultValue
      onChange
      options
      ?noOptionsMessage
    />;
};

module ReactAsyncSelect = {
  [@bs.module "react-select/async"] [@react.component]
  external make:
    (
      ~name: string,
      ~autoFocus: option(bool)=?,
      ~isMulti: option(bool)=?,
      ~isDisabled: option(bool)=?,
      ~isLoading: option(bool)=?,
      ~isSearchable: option(bool)=?,
      ~placeholder: option(string)=?,
      ~noOptionsMessage: optionMessageCb => string=?,
      ~loadingMessage: optionMessageCb => string=?,
      ~className: option(string)=?,
      ~classNamePrefix: option(string)=?,
      ~value: option(labelOption('value))=?,
      ~defaultValue: option('value)=?,
      ~onChange: labelOption('value) => unit,
      ~cacheOptions: option(bool)=?,
      ~defaultOptions: option(array(labelOption('value)))=?,
      ~loadOptions: string =>
                    Promise.Js.t(array(labelOption('value)), string)
    ) =>
    React.element =
    "default";
};

module ReactAsyncSelectMultiple = {
  [@bs.module "react-select/async"] [@react.component]
  external make:
    (
      ~name: string,
      ~autoFocus: option(bool)=?,
      ~isMulti: option(bool)=?,
      ~isDisabled: option(bool)=?,
      ~isLoading: option(bool)=?,
      ~isSearchable: option(bool)=?,
      ~placeholder: option(string)=?,
      ~noOptionsMessage: optionMessageCb => string=?,
      ~loadingMessage: optionMessageCb => string=?,
      ~className: option(string)=?,
      ~classNamePrefix: option(string)=?,
      ~value: option(array(labelOption('value)))=?,
      ~defaultValue: option('value)=?,
      ~onChange: array(labelOption('value)) => unit,
      ~cacheOptions: option(bool)=?,
      ~defaultOptions: option(array(labelOption('value)))=?,
      ~loadOptions: string =>
                    Promise.Js.t(array(labelOption('value)), string)
    ) =>
    React.element =
    "default";
};
