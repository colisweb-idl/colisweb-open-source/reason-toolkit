type t;

external castEvent: 'event => ReactEvent.Synthetic.t = "%identity";

let preventDownshiftDefault: ReactEvent.Synthetic.t => unit = [%bs.raw
  {|
    function (event) {
      event.nativeEvent.preventDownshiftDefault = true;
    }
  |}
];

module Select = {
  type state('item) = {
    isOpen: bool,
    selectedItem: Js.Nullable.t('item),
    inputValue: string,
    highlightedIndex: int,
  };

  type actionString;

  type actionAndChanges('item) = {
    [@bs.as "type"]
    action: actionString,
    changes: state('item),
  };

  type selectedItem('item) = {
    [@bs.as "type"]
    action: actionString,
    selectedItem: 'item,
  };

  type selectOptions;

  [@bs.obj]
  // See more at https://github.com/downshift-js/downshift/tree/master/src/hooks/useSelect
  external selectOptions:
    (
      ~items: array('item),
      ~itemToString: Js.Nullable.t('item) => string,
      ~stateReducer: (state('item), actionAndChanges('item)) => state('item)
                       =?,
      ~onStateChange: actionAndChanges('item) => unit=?,
      ~onSelectedItemChange: selectedItem('item) => unit=?,
      ~defaultIsOpen: bool=?,
      ~isOpen: bool=?,
      ~initialHighlightedIndex: int=?,
      ~defaultHighlightedIndex: int=?,
      ~initialSelectedItem: Js.Nullable.t('item)=?,
      ~defaultSelectedItem: Js.Nullable.t('item)=?,
      ~selectedItem: Js.Nullable.t('item)=?,
      unit
    ) =>
    selectOptions;

  type toggleButtonPropsOptions;
  [@bs.obj]
  external toggleButtonPropsOptions:
    (
      ~id: string=?,
      ~onClick: ReactEvent.Mouse.t => unit=?,
      ~onKeyDown: ReactEvent.Keyboard.t => unit=?,
      ~disabled: bool=?,
      ~ref: option(ReactDOMRe.domRef)=?,
      unit
    ) =>
    toggleButtonPropsOptions;

  type toggleButtonProps = {
    ref: ReactDOMRe.Ref.callbackDomRef,
    id: string,
    onClick: ReactEvent.Mouse.t => unit,
    disabled: bool,
    [@bs.as "aria-labelledby"]
    ariaLabelledby: string,
    [@bs.as "aria-haspopup"]
    ariaHaspopup: string,
    [@bs.as "aria-expanded"]
    ariaExpanded: bool,
  };

  type itemPropsOptions;
  [@bs.obj]
  external itemPropsOptions:
    (
      ~item: 'item,
      ~index: int,
      ~onClick: ReactEvent.Mouse.t => unit=?,
      ~onMouseMove: ReactEvent.Mouse.t => unit=?,
      ~disabled: bool=?,
      ~id: string=?,
      unit
    ) =>
    itemPropsOptions;

  type itemProps = {
    id: string,
    role: string,
    disabled: bool,
    [@bs.as "aria-selected"]
    ariaSelected: string,
    onClick: ReactEvent.Mouse.t => unit,
    onMouseMove: ReactEvent.Mouse.t => unit,
  };

  type labelPropsOptions;
  [@bs.obj]
  external labelPropsOptions:
    (~id: string=?, ~htmlFor: string=?, unit) => labelPropsOptions;
  type labelProps = {
    id: string,
    htmlFor: string,
  };

  type menuPropsOptions;

  [@bs.obj]
  external menuPropsOptions:
    (
      ~onMouseLeave: ReactEvent.Mouse.t => unit=?,
      ~onKeyDown: ReactEvent.Keyboard.t => unit=?,
      ~onBlur: ReactEvent.Focus.t => unit=?,
      ~ref: option(ReactDOMRe.domRef)=?,
      ~id: string=?,
      unit
    ) =>
    menuPropsOptions;

  type menuProps = {
    ref: ReactDOMRe.Ref.callbackDomRef,
    id: string,
    role: string,
    tabIndex: int,
    [@bs.as "aria-labelledby"]
    ariaLabelledby: string,
    [@bs.as "aria-activedescendant"]
    ariaActivedescendant: string,
    onKeyDown: ReactEvent.Keyboard.t => unit,
    onBlur: ReactEvent.Focus.t => unit,
    onMouseLeave: ReactEvent.Mouse.t => unit,
  };

  type t('item) = {
    // prop getters.
    getToggleButtonProps: toggleButtonPropsOptions => toggleButtonProps,
    getLabelProps: labelPropsOptions => labelProps,
    getMenuProps: menuPropsOptions => menuProps,
    getItemProps: itemPropsOptions => itemProps,
    // actions.
    toggleMenu: unit => unit,
    openMenu: unit => unit,
    closeMenu: unit => unit,
    setHighlightedIndex: int => unit,
    setInputValue: string => unit,
    selectItem: Js.Nullable.t('item) => unit,
    reset: unit => unit,
    // state.
    highlightedIndex: int,
    isOpen: bool,
    selectedItem: Js.Nullable.t('item),
    inputValue: string,
  };

  [@bs.module "downshift"]
  external useSelect: selectOptions => t('item) = "useSelect";

  /* State changes and types */

  type instance;
  [@bs.module "downshift"] external useSelectIntance: instance = "useSelect";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external menuKeyDownArrowDown: instance => actionString =
    "MenuKeyDownArrowDown";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external menuKeyDownArrowUp: instance => actionString = "MenuKeyDownArrowUp";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external menuKeyDownEscape: instance => actionString = "MenuKeyDownEscape";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external menuKeyDownHome: instance => actionString = "MenuKeyDownHome";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external menuKeyDownEnd: instance => actionString = "MenuKeyDownEnd";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external menuKeyDownEnter: instance => actionString = "MenuKeyDownEnter";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external menuKeyDownSpaceButton: instance => actionString =
    "MenuKeyDownSpaceButton";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external menuKeyDownCharacter: instance => actionString =
    "MenuKeyDownCharacter";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external menuBlur: instance => actionString = "MenuBlur";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external menuMouseLeave: instance => actionString = "MenuMouseLeave";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external itemMouseMove: instance => actionString = "ItemMouseMove";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external itemClick: instance => actionString = "ItemClick";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external toggleButtonClick: instance => actionString = "ToggleButtonClick";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external toggleButtonKeyDownArrowDown: instance => actionString =
    "ToggleButtonKeyDownArrowDown";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external toggleButtonKeyDownArrowUp: instance => actionString =
    "ToggleButtonKeyDownArrowUp";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external toggleButtonKeyDownCharacter: instance => actionString =
    "ToggleButtonKeyDownCharacter";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionToggleMenu: instance => actionString = "FunctionToggleMenu";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionOpenMenu: instance => actionString = "FunctionOpenMenu";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionCloseMenu: instance => actionString = "FunctionCloseMenu";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionSetHighlightedIndex: instance => actionString =
    "FunctionSetHighlightedIndex";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionSelectItem: instance => actionString = "FunctionSelectItem";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionSetInputValue: instance => actionString =
    "FunctionSetInputValue";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionReset: instance => actionString = "FunctionReset";

  type action =
    | MenuKeyDownArrowDown
    | MenuKeyDownArrowUp
    | MenuKeyDownEscape
    | MenuKeyDownHome
    | MenuKeyDownEnd
    | MenuKeyDownEnter
    | MenuKeyDownSpaceButton
    | MenuKeyDownCharacter
    | MenuBlur
    | MenuMouseLeave
    | ItemMouseMove
    | ItemClick
    | ToggleButtonClick
    | ToggleButtonKeyDownArrowDown
    | ToggleButtonKeyDownArrowUp
    | ToggleButtonKeyDownCharacter
    | FunctionToggleMenu
    | FunctionOpenMenu
    | FunctionCloseMenu
    | FunctionSetHighlightedIndex
    | FunctionSelectItem
    | FunctionSetInputValue
    | FunctionReset;

  let action = actionString =>
    switch (actionString) {
    | action when action === useSelectIntance->menuKeyDownArrowDown =>
      MenuKeyDownArrowDown
    | action when action === useSelectIntance->menuKeyDownArrowUp =>
      MenuKeyDownArrowUp
    | action when action === useSelectIntance->menuKeyDownEscape =>
      MenuKeyDownEscape
    | action when action === useSelectIntance->menuKeyDownHome =>
      MenuKeyDownHome
    | action when action === useSelectIntance->menuKeyDownEnd => MenuKeyDownEnd
    | action when action === useSelectIntance->menuKeyDownEnter =>
      MenuKeyDownEnter
    | action when action === useSelectIntance->menuKeyDownSpaceButton =>
      MenuKeyDownSpaceButton
    | action when action === useSelectIntance->menuKeyDownCharacter =>
      MenuKeyDownCharacter
    | action when action === useSelectIntance->menuBlur => MenuBlur
    | action when action === useSelectIntance->menuMouseLeave => MenuMouseLeave
    | action when action === useSelectIntance->itemMouseMove => ItemMouseMove
    | action when action === useSelectIntance->itemClick => ItemClick
    | action when action === useSelectIntance->toggleButtonClick =>
      ToggleButtonClick
    | action when action === useSelectIntance->toggleButtonKeyDownArrowDown =>
      ToggleButtonKeyDownArrowDown
    | action when action === useSelectIntance->toggleButtonKeyDownArrowUp =>
      ToggleButtonKeyDownArrowUp
    | action when action === useSelectIntance->toggleButtonKeyDownCharacter =>
      ToggleButtonKeyDownCharacter
    | action when action === useSelectIntance->functionToggleMenu =>
      FunctionToggleMenu
    | action when action === useSelectIntance->functionOpenMenu =>
      FunctionOpenMenu
    | action when action === useSelectIntance->functionCloseMenu =>
      FunctionCloseMenu
    | action when action === useSelectIntance->functionSetHighlightedIndex =>
      FunctionSetHighlightedIndex
    | action when action === useSelectIntance->functionSelectItem =>
      FunctionSelectItem
    | action when action === useSelectIntance->functionSetInputValue =>
      FunctionSetInputValue
    | action when action === useSelectIntance->functionReset => FunctionReset
    | _ => Js.Exn.raiseError("unhandled BsDownShift actionString")
    };
};

module Combobox = {
  type state('item) = {
    isOpen: bool,
    selectedItem: Js.Nullable.t('item),
    inputValue: string,
    highlightedIndex: int,
  };

  type actionString;

  type actionAndChanges('item) = {
    [@bs.as "type"]
    action: actionString,
    changes: state('item),
  };

  type selectedItem('item) = {
    [@bs.as "type"]
    action: actionString,
    selectedItem: 'item,
  };

  type comboboxOptions;

  [@bs.obj]
  // See more at https://github.com/downshift-js/downshift/tree/master/src/hooks/useCombobox
  external comboboxOptions:
    (
      ~items: array('item),
      ~itemToString: Js.Nullable.t('item) => string,
      ~stateReducer: (state('item), actionAndChanges('item)) => state('item)
                       =?,
      ~onStateChange: actionAndChanges('item) => unit=?,
      ~onSelectedItemChange: selectedItem('item) => unit=?,
      ~defaultIsOpen: bool=?,
      ~isOpen: bool=?,
      ~initialInputValue: string=?,
      ~inputValue: string=?,
      ~initialSelectedItem: 'item=?,
      ~highlightedIndex: int=?,
      ~initialHighlightedIndex: int=?,
      ~defaultHighlightedIndex: int=?,
      ~initialSelectedItem: Js.Nullable.t('item)=?,
      ~defaultSelectedItem: Js.Nullable.t('item)=?,
      ~selectedItem: Js.Null.t('item)=?,
      unit
    ) =>
    comboboxOptions;

  type comboboxPropsOptions;
  [@bs.obj]
  external comboboxPropsOptions:
    (~id: string=?, ~ref: option(ReactDOMRe.domRef)=?, unit) =>
    comboboxPropsOptions;

  type comboboxProps = {
    ref: ReactDOMRe.Ref.callbackDomRef,
    role: string,
    [@bs.as "aria-haspopup"]
    ariaHaspopup: string,
    [@bs.as "aria-expanded"]
    ariaExpanded: bool,
    [@bs.as "aria-owns"]
    ariaOwns: string,
  };

  type toggleButtonPropsOptions;
  [@bs.obj]
  external toggleButtonPropsOptions:
    (
      ~id: string=?,
      ~onClick: ReactEvent.Mouse.t => unit=?,
      ~disabled: bool=?,
      ~ref: option(ReactDOMRe.domRef)=?,
      unit
    ) =>
    toggleButtonPropsOptions;

  type toggleButtonProps = {
    ref: ReactDOMRe.Ref.callbackDomRef,
    id: string,
    onClick: ReactEvent.Mouse.t => unit,
    disabled: bool,
    tabIndex: int,
  };

  type itemPropsOptions;
  [@bs.obj]
  external itemPropsOptions:
    (
      ~item: 'item,
      ~index: int,
      ~id: string=?,
      ~onClick: ReactEvent.Mouse.t => unit=?,
      ~onMouseMove: ReactEvent.Mouse.t => unit=?,
      ~disabled: bool=?,
      ~ref: option(ReactDOMRe.domRef)=?,
      unit
    ) =>
    itemPropsOptions;

  type itemProps = {
    ref: ReactDOMRe.Ref.callbackDomRef,
    id: string,
    role: string,
    disabled: bool,
    [@bs.as "aria-selected"]
    ariaSelected: string,
    onClick: ReactEvent.Mouse.t => unit,
    onMouseMove: ReactEvent.Mouse.t => unit,
  };

  type labelPropsOptions;
  [@bs.obj]
  external labelPropsOptions:
    (~id: string=?, ~htmlFor: string=?, unit) => labelPropsOptions;

  type labelProps = {
    id: string,
    htmlFor: string,
  };

  type menuPropsOptions;

  [@bs.obj]
  external menuPropsOptions:
    (
      ~onMouseLeave: ReactEvent.Mouse.t => unit=?,
      ~ref: option(ReactDOMRe.domRef)=?,
      ~id: string=?,
      unit
    ) =>
    menuPropsOptions;

  type menuProps = {
    ref: ReactDOMRe.Ref.callbackDomRef,
    id: string,
    role: string,
    [@bs.as "aria-labelledby"]
    ariaLabelledby: string,
    onMouseLeave: ReactEvent.Mouse.t => unit,
  };

  type inputPropsOptions;
  [@bs.obj]
  external inputPropsOptions:
    (
      ~ref: option(ReactDOMRe.domRef)=?,
      ~id: string=?,
      ~onBlur: ReactEvent.Focus.t => unit=?,
      ~onKeyDown: ReactEvent.Keyboard.t => unit=?,
      ~onChange: ReactEvent.Form.t => unit=?,
      ~disabled: bool=?,
      unit
    ) =>
    inputPropsOptions;

  type inputProps = {
    ref: ReactDOMRe.Ref.callbackDomRef,
    id: string,
    [@bs.as "aria-controls"]
    ariaControls: string,
    [@bs.as "aria-activedescendant"]
    ariaActivedescendant: string,
    [@bs.as "aria-labelledby"]
    ariaLabelledby: string,
    [@bs.as "aria-autocomplete"]
    ariaAutocomplete: string,
    disabled: bool,
    value: string,
    autoComplete: string,
    onBlur: ReactEvent.Focus.t => unit,
    onKeyDown: ReactEvent.Keyboard.t => unit,
    onChange: ReactEvent.Form.t => unit,
  };

  type t('item) = {
    // prop getters.
    getToggleButtonProps: toggleButtonPropsOptions => toggleButtonProps,
    getLabelProps: labelPropsOptions => labelProps,
    getMenuProps: menuPropsOptions => menuProps,
    getItemProps: itemPropsOptions => itemProps,
    getInputProps: inputPropsOptions => inputProps,
    getComboboxProps: comboboxPropsOptions => comboboxProps,
    // actions.
    toggleMenu: unit => unit,
    openMenu: unit => unit,
    closeMenu: unit => unit,
    setHighlightedIndex: int => unit,
    setInputValue: string => unit,
    selectItem: Js.Nullable.t('item) => unit,
    reset: unit => unit,
    // state.
    highlightedIndex: int,
    isOpen: bool,
    selectedItem: Js.Nullable.t('item),
    inputValue: string,
  };

  [@bs.module "downshift"]
  external useCombobox: comboboxOptions => t('item) = "useCombobox";

  /* State changes and types */

  type instance;
  [@bs.module "downshift"]
  external useComboboxIntance: instance = "useCombobox";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external inputKeyDownArrowDown: instance => actionString =
    "InputKeyDownArrowDown";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external inputKeyDownArrowUp: instance => actionString =
    "InputKeyDownArrowUp";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external inputKeyDownEscape: instance => actionString = "InputKeyDownEscape";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external inputKeyDownHome: instance => actionString = "InputKeyDownHome";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external inputKeyDownEnd: instance => actionString = "InputKeyDownEnd";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external inputKeyDownEnter: instance => actionString = "InputKeyDownEnter";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external inputChange: instance => actionString = "InputChange";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external inputBlur: instance => actionString = "InputBlur";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external menuMouseLeave: instance => actionString = "MenuMouseLeave";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external itemMouseMove: instance => actionString = "ItemMouseMove";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external itemClick: instance => actionString = "ItemClick";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external toggleButtonClick: instance => actionString = "ToggleButtonClick";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionToggleMenu: instance => actionString = "FunctionToggleMenu";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionOpenMenu: instance => actionString = "FunctionOpenMenu";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionCloseMenu: instance => actionString = "FunctionCloseMenu";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionSetHighlightedIndex: instance => actionString =
    "FunctionSetHighlightedIndex";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionSelectItem: instance => actionString = "FunctionSelectItem";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionSetInputValue: instance => actionString =
    "FunctionSetInputValue";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external functionReset: instance => actionString = "FunctionReset";

  [@bs.get] [@bs.scope "stateChangeTypes"]
  external controlledPropUpdatedSelectedItem: instance => actionString =
    "ControlledPropUpdatedSelectedItem";

  type action =
    | InputKeyDownArrowDown
    | InputKeyDownArrowUp
    | InputKeyDownEscape
    | InputKeyDownHome
    | InputKeyDownEnd
    | InputKeyDownEnter
    | InputChange
    | InputBlur
    | MenuMouseLeave
    | ItemMouseMove
    | ItemClick
    | ToggleButtonClick
    | FunctionToggleMenu
    | FunctionOpenMenu
    | FunctionCloseMenu
    | FunctionSetHighlightedIndex
    | FunctionSelectItem
    | FunctionSetInputValue
    | FunctionReset
    | ControlledPropUpdatedSelectedItem;

  let action = actionString =>
    switch (actionString) {
    | action when action === useComboboxIntance->inputKeyDownArrowDown =>
      InputKeyDownArrowDown
    | action when action === useComboboxIntance->inputKeyDownArrowUp =>
      InputKeyDownArrowUp
    | action when action === useComboboxIntance->inputKeyDownEscape =>
      InputKeyDownEscape
    | action when action === useComboboxIntance->inputKeyDownHome =>
      InputKeyDownHome
    | action when action === useComboboxIntance->inputKeyDownEnd =>
      InputKeyDownEnd
    | action when action === useComboboxIntance->inputKeyDownEnter =>
      InputKeyDownEnter
    | action when action === useComboboxIntance->inputChange => InputChange
    | action when action === useComboboxIntance->inputBlur => InputBlur
    | action when action === useComboboxIntance->menuMouseLeave =>
      MenuMouseLeave
    | action when action === useComboboxIntance->itemMouseMove => ItemMouseMove
    | action when action === useComboboxIntance->itemClick => ItemClick
    | action when action === useComboboxIntance->toggleButtonClick =>
      ToggleButtonClick
    | action when action === useComboboxIntance->functionToggleMenu =>
      FunctionToggleMenu
    | action when action === useComboboxIntance->functionOpenMenu =>
      FunctionOpenMenu
    | action when action === useComboboxIntance->functionCloseMenu =>
      FunctionCloseMenu
    | action when action === useComboboxIntance->functionSetHighlightedIndex =>
      FunctionSetHighlightedIndex
    | action when action === useComboboxIntance->functionSelectItem =>
      FunctionSelectItem
    | action when action === useComboboxIntance->functionSetInputValue =>
      FunctionSetInputValue
    | action when action === useComboboxIntance->functionReset => FunctionReset
    | action
        when action === useComboboxIntance->controlledPropUpdatedSelectedItem =>
      ControlledPropUpdatedSelectedItem
    | _ => Js.Exn.raiseError("unhandled BsDownShift actionString")
    };
};
