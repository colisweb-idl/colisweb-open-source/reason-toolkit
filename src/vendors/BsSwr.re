type revalidateOptions;

[@bs.obj]
external revalidateOptions:
  (~retryCount: option(int)=?, ~dedupe: option(bool)=?, unit) =>
  revalidateOptions;

type fetcherOptions;

[@bs.obj]
external fetcherOptions:
  (
    ~suspense: bool=?,
    ~fetcher: 'fetcher=?,
    ~initialData: 'data=?,
    ~revalidateOnFocus: bool=?,
    ~revalidateOnMount: bool=?,
    ~revalidateOnReconnect: bool=?,
    ~refreshInterval: int=?,
    ~refreshWhenHidden: bool=?,
    ~refreshWhenOffline: bool=?,
    ~shouldRetryOnError: bool=?,
    ~dedupingInterval: int=?,
    ~focusThrottleInterval: int=?,
    ~loadingTimeout: int=?,
    ~errorRetryInterval: int=?,
    ~onLoadingSlow: (string, fetcherOptions) => unit=?,
    ~onSuccess: ('data, string, fetcherOptions) => unit=?,
    ~onError: (Js.Exn.t, string, fetcherOptions) => unit=?,
    ~onErrorRetry: (
                     Js.Exn.t,
                     string,
                     fetcherOptions,
                     revalidateOptions => Js.Promise.t(bool),
                     revalidateOptions
                   ) =>
                   unit
                     =?,
    ~compare: (Js.Nullable.t('data), Js.Nullable.t('data)) => bool=?,
    unit
  ) =>
  fetcherOptions;

type fetcher('data) = {
  data: option('data),
  error: option(Js.Exn.t),
  isValidating: bool,
  revalidate: revalidateOptions => Js.Promise.t(bool),
};

[@bs.module "swr"]
external useSwr: ('key, 'fn, 'fetcherOptions) => fetcher('data) = "default";

[@bs.module "swr"]
external useSwrOptional:
  ('key, 'fn, 'fetcherOptions) => fetcher(option('data)) =
  "default";

[@bs.module "swr"] external trigger: 'key => unit = "trigger";

[@bs.module "swr"] external mutate: ('key, 'data, bool) => unit = "mutate";

type cache;
[@bs.module "swr"] external cache: cache = "cache";
[@bs.send] external clear: cache => unit = "clear";
[@bs.send] external delete: (cache, 'key) => unit = "delete";
