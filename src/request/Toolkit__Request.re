type config;
type headers;
type undecodedData;
type failedResponseData;

type undecodedResponse = {
  data: undecodedData,
  headers,
  config,
  status: int,
};

type noResponse = {
  message: string,
  config,
};

type failedResponse = {
  message: string,
  config,
  response,
}
and response = {
  data: failedResponseData,
  headers,
  status: int,
};

external castToJs: 'a => Js.t('b) = "%identity";
external castToFailedResponse: Js.Promise.error => failedResponse =
  "%identity";
external castToNoResponse: Js.Promise.error => noResponse = "%identity";
external castToUndecodedResponse: Js.t('a) => undecodedResponse = "%identity";

type error('a) = [
  | `noResponse(noResponse)
  | `invalidResponse(failedResponse)
  | `invalidResponseData(undecodedResponse, string)
  | `invalidErrorData
  | `unknown(Js.Promise.error)
  | `custom('a)
];

let toResult =
    (
      promise,
      ~mapError: option(failedResponse => option(result('data, error('a))))=?,
      ~errorDecoder=?,
      mapData: Js.Json.t => result('data, Decco.decodeError),
    )
    : Promise.promise(result('data, error('error))) =>
  promise
  ->Promise.Js.fromBsPromise
  ->Promise.Js.toResult
  ->Promise.map(res => {
      switch (res) {
      | Ok(response) =>
        switch (response##data->mapData) {
        | Ok(_) as ok => ok
        | Error(error) =>
          Error(
            `invalidResponseData((
              response->castToUndecodedResponse,
              "\"" ++ error.path ++ "\" " ++ error.message,
            )),
          )
        }
      | Error(error) =>
        if (error->castToJs##response) {
          switch (mapError, errorDecoder) {
          | (_, Some(errorDecoder)) =>
            let error =
              error->castToFailedResponse.response.data
              ->Obj.magic
              ->errorDecoder;

            switch (error) {
            | Ok(err) => Error(`custom(err))
            | Error(_err) =>
              [%log.error "errorDecoder"; ("err", _err)];
              Error(`invalidErrorData);
            };

          | (Some(mapError), _) =>
            switch (error->castToFailedResponse->mapError) {
            | None => Error(`invalidResponse(error->castToFailedResponse))
            | Some(result) => result
            }
          | _ => Error(`invalidResponse(error->castToFailedResponse))
          };
        } else if (error->castToJs##request) {
          Error(`noResponse(error->castToNoResponse));
        } else {
          Error(`unknown(error));
        }
      }
    });

type requestError('a) = error('a);

module type Config = {
  type argument;
  type response;
  type error;
  let exec:
    argument => Promise.promise(result(response, requestError(error)));
};

module Make = (Config: Config) => {
  include Config;
};

/* Http method */

let get =
    (
      request: Axios.Instance.t,
      url: string,
      ~mapError: option(failedResponse => option(result('data, error('a))))=?,
      ~errorDecoder=?,
      mapData: Js.Json.t => result('response, Decco.decodeError),
    )
    : Promise.promise(result('response, error('error))) => {
  request
  ->Axios.Instance.get(url)
  ->toResult(~mapError?, ~errorDecoder?, mapData);
};

let getc =
    (
      request: Axios.Instance.t,
      url: string,
      ~mapError: option(failedResponse => option(result('data, error('a))))=?,
      ~errorDecoder=?,
      ~config,
      mapData: Js.Json.t => result('response, Decco.decodeError),
    )
    : Promise.promise(result('response, error('error))) => {
  request
  ->Axios.Instance.getc(url, config)
  ->toResult(~mapError?, ~errorDecoder?, mapData);
};

let post =
    (
      request: Axios.Instance.t,
      url: string,
      ~mapError: option(failedResponse => option(result('data, error('a))))=?,
      ~errorDecoder=?,
      mapData: Js.Json.t => result('response, Decco.decodeError),
    )
    : Promise.promise(result('response, error('error))) => {
  request
  ->Axios.Instance.post(url)
  ->toResult(~mapError?, ~errorDecoder?, mapData);
};

let postData =
    (
      request: Axios.Instance.t,
      url: string,
      ~mapError: option(failedResponse => option(result('data, error('a))))=?,
      ~errorDecoder=?,
      mapData: Js.Json.t => result('response, Decco.decodeError),
      body: Js.Json.t,
    )
    : Promise.promise(result('response, error('error))) => {
  request
  ->Axios.Instance.postData(url, body->castToJs)
  ->toResult(~mapError?, ~errorDecoder?, mapData);
};

let put =
    (
      request: Axios.Instance.t,
      url: string,
      ~mapError: option(failedResponse => option(result('data, error('a))))=?,
      ~errorDecoder=?,
      mapData: Js.Json.t => result('response, Decco.decodeError),
    )
    : Promise.promise(result('response, error('error))) => {
  request
  ->Axios.Instance.put(url)
  ->toResult(~mapError?, ~errorDecoder?, mapData);
};

let putData =
    (
      request: Axios.Instance.t,
      url: string,
      ~mapError: option(failedResponse => option(result('data, error('a))))=?,
      ~errorDecoder=?,
      mapData: Js.Json.t => result('response, Decco.decodeError),
      body: Js.Json.t,
    )
    : Promise.promise(result('response, error('error))) => {
  request
  ->Axios.Instance.putData(url, body->castToJs)
  ->toResult(~mapError?, ~errorDecoder?, mapData);
};

let delete =
    (
      request: Axios.Instance.t,
      url: string,
      ~mapError: option(failedResponse => option(result('data, error('a))))=?,
      ~errorDecoder=?,
      mapData: Js.Json.t => result('response, Decco.decodeError),
    )
    : Promise.promise(result('response, error('error))) => {
  request
  ->Axios.Instance.delete(url)
  ->toResult(~mapError?, ~errorDecoder?, mapData);
};

module Error = {
  type errorType = [
    | `overlappingActivations(array(activation))
    | `compileError(string)
    | `accessRefused
    | `unknown
  ]
  [@decco]
  and activation = {ruleTitle: string};

  let encoder: Decco.encoder(errorType) = _v => ""->Decco.stringToJson;

  let decoder: Decco.decoder(errorType) =
    json => {
      let error = json->Obj.magic;
      let value =
        switch (error##code) {
        | Some("OVERLAPPING_ACTIVATIONS") =>
          `overlappingActivations(
            error##overlapped
            ->Obj.magic
            ->Array.map(a => a->activation_decode->Result.getExn),
          )

        | Some("COMPILE_ERROR") =>
          `compileError(error##compileError->Obj.magic)
        | Some("ACCESS_REFUSED") => `accessRefused

        | _ => `unknown
        };

      value->Ok;
    };

  let codec: Decco.codec(errorType) = (encoder, decoder);

  [@decco]
  type t = [@decco.codec codec] errorType;
};
