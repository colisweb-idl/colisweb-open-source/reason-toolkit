# Request

## Usage

```reasonml
module API = {
  module FetchUsers = {
    module Config = {
      type argument = unit;

      [@decco]
      type response = array(user)
      [@decco]
      and user = {
        name: string
      };

      type error = string;

      let exec = clientId => {
        request->Request.get(baseUrl(~clientId), response_decode);
      };
    };

    module Request = Toolkit.Request.Make(Config);
  };
};

API.FetchUsers.Request.exec();
```

### Handle custom error

```reasonml
module API = {
  module FetchUsers = {
    module Error = {
      type errorType = [
        | `overlappingActivations(array(activation))
        | `compileError(string)
        | `accessRefused
        | `unknown
      ]
      [@decco]
      and activation = {ruleTitle: string};

      let encoder: Decco.encoder(errorType) = _v => ""->Decco.stringToJson;

      let decoder: Decco.decoder(errorType) =
        json => {
          let error = json->Obj.magic;
          let value =
            switch (error##code) {
            | Some("OVERLAPPING_ACTIVATIONS") =>
              `overlappingActivations(
                error##overlapped
                ->Obj.magic
                ->Array.map(a => a->activation_decode->Result.getExn),
              )
            | Some("COMPILE_ERROR") =>
              `compileError(error##compileError->Obj.magic)
            | Some("ACCESS_REFUSED") => `accessRefused
            | _ => `unknown
            };

          value->Ok;
        };

      let codec: Decco.codec(errorType) = (encoder, decoder);

      [@decco]
      type t = [@decco.codec codec] errorType;
    };

    module Config = {
      type argument = unit;

      [@decco]
      type response = array(user)
      [@decco]
      and user = {
        name: string
      };

      [@decco]
      type error = Error.t;

      let exec = clientId => {
        request->Request.get(baseUrl(~clientId), ~errorDecoder=error_decode,response_decode);
      };
    };

    module Request = Toolkit.Request.Make(Config);
  };
};

API.FetchUsers.Request.exec()
->Promise.tapError(err => switch (err) {
  | Custom(`accessRefused) => "handle refused access"
  | Custom(`compileError(msg)) => msg
  | _ => "else"
});
```
