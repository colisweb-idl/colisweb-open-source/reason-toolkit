module Date = {
  type date = Js.Date.t;

  let encoder: Decco.encoder(date) =
    value => value->Js.Date.toISOString->Decco.stringToJson;

  let decoder: Decco.decoder(date) =
    json => {
      switch (Decco.stringFromJson(json)) {
      | Ok(value) => Ok(value->Js.Date.fromString)
      | Error(_) as error => error
      };
    };

  let codec: Decco.codec(date) = (encoder, decoder);

  [@decco]
  type t = [@decco.codec codec] date;
};

module Int = {
  let encoder = value => value->Decco.intToJson;

  let decoder = json => {
    switch (Js.Json.classify(json)) {
    | JSONNumber(_) =>
      switch (Decco.intFromJson(json)) {
      | Ok(_) as value => value
      | Error(_) as error => error
      }
    | JSONString(_) =>
      switch (Decco.stringFromJson(json)) {
      | Ok(value) =>
        switch (Decco.intFromJson(value->Js.Float.fromString->Js.Json.number)) {
        | Ok(_) as value => value
        | Error(_) as error => error
        }
      | Error(_) => Decco.error(~path="", "Not an integer", json)
      }
    | _ => Decco.error(~path="", "Not a number", json)
    };
  };

  let codec = (encoder, decoder);

  [@decco]
  type t = [@decco.codec codec] int;
};

module Float = {
  let encoder = value => value->Decco.floatToJson;

  let decoder = json => {
    switch (Js.Json.classify(json)) {
    | JSONNumber(_) =>
      switch (Decco.floatFromJson(json)) {
      | Ok(_) as value => value
      | Error(_) as error => error
      }
    | JSONString(_) =>
      switch (Decco.stringFromJson(json)) {
      | Ok(value) =>
        switch (
          Decco.floatFromJson(value->Js.Float.fromString->Js.Json.number)
        ) {
        | Ok(_) as value => value
        | Error(_) as error => error
        }
      | Error(_) => Decco.error(~path="", "Not a number", json)
      }
    | _ => Decco.error(~path="", "Not a number", json)
    };
  };

  let codec = (encoder, decoder);

  [@decco]
  type t = [@decco.codec codec] float;
};

module type EnumConfig = {
  type enum;
  let enumToJs: enum => string;
  let enumFromJs: string => option(enum);
};

module Enum = (Config: EnumConfig) => {
  type enum = Config.enum;

  let encoder = value => value->Config.enumToJs->Decco.stringToJson;

  let decoder = json => {
    switch (Decco.stringFromJson(json)) {
    | Ok(value) =>
      switch (Config.enumFromJs(value)) {
      | None => Decco.error(~path="", "Invalid enum " ++ value, json)
      | Some(value) => Ok(value)
      }
    | Error(_) as error => error
    };
  };

  let codec = (encoder, decoder);

  let toString = v => v->Config.enumToJs;
  let fromString = v => v->Config.enumFromJs;

  [@decco]
  type t = [@decco.codec codec] enum;
};

module Option = {
  let encoder = (encoder, value) =>
    switch (value) {
    | None =>
      %bs.raw
      "undefined"
    | Some(value) => encoder(value)
    };

  let decoder = (decoder, json) =>
    json !== [%bs.raw "undefined"]
      ? Decco.optionFromJson(decoder, json) : Ok(None);

  let codec = (encoder, decoder);

  [@decco]
  type t('a) = [@decco.codec (encoder, decoder)] option('a);
};

module UnitMeasure = {
  module Dimension = {
    module WithUnit = {
      let encoder: Decco.encoder(Toolkit__Utils_UnitMeasure.Dimension.t) =
        value =>
          value
          ->Toolkit__Utils_UnitMeasure.Dimension.toString()
          ->Decco.stringToJson;

      let decoder: Decco.decoder(Toolkit__Utils_UnitMeasure.Dimension.t) =
        json => {
          switch (Decco.stringFromJson(json)) {
          | Belt.Result.Ok(v) =>
            let valueUnitArray = v |> Js.String.split(" ");
            switch (valueUnitArray->Array.get(1)) {
            | Some("mm") =>
              `mm(valueUnitArray->Array.getExn(0)->Js.Float.fromString)->Ok
            | Some("cm") =>
              `cm(valueUnitArray->Array.getExn(0)->Js.Float.fromString)->Ok
            | Some("dm") =>
              `dm(valueUnitArray->Array.getExn(0)->Js.Float.fromString)->Ok
            | Some("km") =>
              `km(valueUnitArray->Array.getExn(0)->Js.Float.fromString)->Ok
            | None => raise(Js.Exn.raiseError("unhandled no unit"))
            | _ => raise(Js.Exn.raiseError("unhandled weight unit"))
            };
          | Belt.Result.Error(_) as err => err
          };
        };

      let codec: Decco.codec(Toolkit__Utils_UnitMeasure.Dimension.t) = (
        encoder,
        decoder,
      );

      [@decco]
      type t = [@decco.codec codec] Toolkit__Utils_UnitMeasure.Dimension.t;
    };

    module type DimensionConfig = {
      let convert: float => Toolkit__Utils_UnitMeasure.Dimension.t;
    };
    module Make = (C: DimensionConfig) => {
      type dimension = Toolkit__Utils_UnitMeasure.Dimension.t;

      let encoder: Decco.encoder(Toolkit__Utils_UnitMeasure.Dimension.t) =
        value =>
          value
          ->Toolkit__Utils_UnitMeasure.Dimension.toString()
          ->Decco.stringToJson;

      let decoder: Decco.decoder(Toolkit__Utils_UnitMeasure.Dimension.t) =
        json => {
          switch (Decco.floatFromJson(json)) {
          | Ok(v) => v->C.convert->Ok

          | Error(_) as err => err
          };
        };

      let codec: Decco.codec(dimension) = (encoder, decoder);

      [@decco]
      type t = [@decco.codec codec] Toolkit__Utils_UnitMeasure.Dimension.t;
    };

    module Mm =
      Make({
        let convert = Toolkit__Utils_UnitMeasure.Dimension.makeMm;
      });
    module Cm =
      Make({
        let convert = Toolkit__Utils_UnitMeasure.Dimension.makeCm;
      });
    module Dm =
      Make({
        let convert = Toolkit__Utils_UnitMeasure.Dimension.makeDm;
      });
    module Km =
      Make({
        let convert = Toolkit__Utils_UnitMeasure.Dimension.makeKm;
      });
  };

  module Weight = {
    module WithUnit = {
      let encoder: Decco.encoder(Toolkit__Utils_UnitMeasure.Weight.t) =
        value =>
          value
          ->Toolkit__Utils_UnitMeasure.Weight.display()
          ->Decco.stringToJson;

      let decoder: Decco.decoder(Toolkit__Utils_UnitMeasure.Weight.t) =
        json => {
          switch (Decco.stringFromJson(json)) {
          | Ok(v) =>
            let valueUnitArray = v |> Js.String.split(" ");
            switch (valueUnitArray->Array.get(1)) {
            | Some("g") =>
              `g(valueUnitArray->Array.getExn(0)->Js.Float.fromString)->Ok
            | Some("kg") =>
              `kg(valueUnitArray->Array.getExn(0)->Js.Float.fromString)->Ok
            | None => raise(Js.Exn.raiseError("unhandled no unit"))
            | _ => raise(Js.Exn.raiseError("unhandled weight unit"))
            };

          | Error(_) as err => err
          };
        };

      let codec: Decco.codec(Toolkit__Utils_UnitMeasure.Weight.t) = (
        encoder,
        decoder,
      );

      [@decco]
      type t = [@decco.codec codec] Toolkit__Utils_UnitMeasure.Weight.t;
    };

    module type WeightConfig = {
      let convert: float => Toolkit__Utils_UnitMeasure.Weight.t;
    };

    module Make = (C: WeightConfig) => {
      type weight = Toolkit__Utils_UnitMeasure.Weight.t;

      let encoder: Decco.encoder(Toolkit__Utils_UnitMeasure.Weight.t) =
        value =>
          value
          ->Toolkit__Utils_UnitMeasure.Weight.display()
          ->Decco.stringToJson;

      let decoder: Decco.decoder(Toolkit__Utils_UnitMeasure.Weight.t) =
        json => {
          switch (Decco.floatFromJson(json)) {
          | Ok(v) => v->C.convert->Ok
          | Error(_) as err => err
          };
        };

      let codec: Decco.codec(weight) = (encoder, decoder);

      [@decco]
      type t = [@decco.codec codec] Toolkit__Utils_UnitMeasure.Weight.t;
    };

    module G =
      Make({
        let convert = Toolkit__Utils_UnitMeasure.Weight.makeG;
      });
    module Kg =
      Make({
        let convert = Toolkit__Utils_UnitMeasure.Weight.makeKg;
      });
  };
};

// StringArray
//  This coddec is intended for encoding the array params of API requests of scala services
//  Ex decode: Js.Json.string("1 cm,40 m,3 km") => [|`cm(1),`m(40),`km(3)|]: array(Toolkit.Utils.UnitMeasure.Dimension.t)
//  Ex encode: [|`cm(1.),`m(40.),`km(3.)|] => "1.00 cm,40.00 m,3.00 km": Js.Json.t

module StringArray = {
  let encoder = (encoder, value) =>
    value->Array.map(encoder) |> Js.Array.joinWith(",") |> Decco.stringToJson;

  let decoder = (decoder, json) =>
    switch (json |> Decco.stringFromJson) {
    | Ok(string) =>
      let resultArray =
        (string |> Js.String.split(","))
        ->Array.map(e => e->Js.Json.string->decoder);
      let error = resultArray->Array.getBy(Result.isError);
      switch (error) {
      | Some(_) => Decco.error(~path="", "Invalid content", json)
      | None => Ok(resultArray->Array.map(Result.getExn))
      };
    | Error(_) as error => error
    };

  let codec = (encoder, decoder);

  [@decco]
  type t('a) = [@decco.codec (encoder, decoder)] array('a);
};
