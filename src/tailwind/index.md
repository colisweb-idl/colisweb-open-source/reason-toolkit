# Tailwind

## Usage

```reasonml
[@react.component]
let make = () => {
  <div className="bg-white p-4">
    <p className="text-blue-500">
      "some text"->React.string
    </p>
  </div>
}
```

## Update the configuration

If you want to customize the config, just import it in your own files :

```javascript
/* tailwind.config.js */
const toolkitConfig = require("@colisweb/reason-toolkit/src/tailwind/tailwind.config");

module.exports = {
  ...toolkitConfig,
  variants: {
    ...toolkitConfig.variants,
    width: ["responsive", "hover"],
  },
};
```
