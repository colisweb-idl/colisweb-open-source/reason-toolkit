const { colors: defaultColors } = require("tailwindcss/defaultTheme");
const plugin = require("tailwindcss/plugin");

const flexGapPlugin = plugin(({ addUtilities, theme, variants }) => {
  const newUtilities = Object.keys(theme("spacing")).reduce(
    (acc, key) => ({
      ...acc,
      [`.flex-gap-${key}`]: {
        marginBottom: "-" + theme(`spacing.${key}`),
        marginLeft: "-" + theme(`spacing.${key}`),
        [` > *`]: {
          paddingBottom: theme(`spacing.${key}`),
          paddingLeft: theme(`spacing.${key}`),
        },
      },
      [`.flex-col-gap-${key}`]: {
        marginLeft: "-" + theme(`spacing.${key}`),
        [` > *`]: {
          paddingLeft: theme(`spacing.${key}`),
        },
      },
      [`.flex-row-gap-${key}`]: {
        marginBottom: "-" + theme(`spacing.${key}`),
        [` > *`]: {
          paddingBottom: theme(`spacing.${key}`),
        },
      },
    }),
    {}
  );

  addUtilities(newUtilities, variants("flexGap"));
});

const boxShadowColorPlugin = plugin(({ addUtilities, theme, variants }) => {
  const newUtilities = Object.keys(theme("colors")).reduce(
    (acc, key) =>
      theme("colors")[key][500]
        ? {
            ...acc,
            [`.shadow-${key}-500`]: {
              boxShadow: `${theme("colors")[key][500]} 0px 0px 0px 1px;`,
            },
          }
        : acc,
    {}
  );
  addUtilities(newUtilities, variants("boxShadowColor"));
});

module.exports = {
  mode: "jit",
  theme: {
    extend: {
      screens: {
        print: { raw: "print" },
      },
      inset: {
        "1/2": "50%",
      },
      boxShadow: {
        sidenav: "inset 0 0 1px 2px rgba(0, 0, 0, 0.2)",
        sm:
          "0px 2px 1px -1px rgba(0,0,0,0.1), 0px 1px 1px 0px rgba(0,0,0,0.1), 0px 1px 3px 0px rgba(0,0,0,0.1)",
      },
      borderColor: {
        currentColor: "currentColor",
      },
      gridTemplateColumns: {
        xs: "repeat(auto-fit, minmax(8rem, 1fr))",
        sm: "repeat(auto-fit, minmax(14rem, 1fr))",
        md: "repeat(auto-fit, minmax(20rem, 1fr))",
        lg: "repeat(auto-fit, minmax(28rem, 1fr))",
        xl: "repeat(auto-fit, minmax(36rem, 1fr))",
      },
    },
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
    },
    fontFamily: {
      display: ["Montserrat", "sans-serif"],
      sans: ["Open Sans", "sans-serif"],
      mono: ["Roboto Mono", "monospace"],
      code: ["Fira Code", "monospace"],
    },
    fontSize: {
      xs: "0.75rem",
      sm: "0.875rem",
      base: "1rem",
      lg: "1.125rem",
      xl: "1.25rem",
      "2xl": "1.5rem",
      "3xl": "1.875rem",
      "4xl": "2.25rem",
      "5xl": "3rem",
      "6xl": "4rem",
    },
    maxHeight: {
      none: "none",
      xs: "20rem",
      sm: "24rem",
      md: "28rem",
      lg: "32rem",
      xl: "36rem",
      "2xl": "42rem",
      "3xl": "48rem",
      "4xl": "56rem",
      "5xl": "64rem",
      "6xl": "72rem",
      full: "100%",
      screen: "100vh",
    },
    colors: {
      white: defaultColors.white,
      black: defaultColors.black,
      transparent: defaultColors.transparent,
      gray: {
        100: "#f7fafc",
        200: "#edf2f7",
        300: "#e2e8f0",
        400: "#cbd5e0",
        500: "#a0aec0",
        600: "#718096",
        700: "#4a5568",
        800: "#2d3748",
        900: "#1a202c",
      },
      primary: {
        50: "#EDFDFF",
        100: "#C9FAFF",
        200: "#A4F7FF",
        300: "#6DECFD",
        400: "#3DDEF3",
        500: "#15CBE3",
        600: "#1BB0C4",
        700: "#208E9C",
        800: "#1F646D",
        900: "#16363B",
      },
      neutral: {
        100: "#F2F9F9",
        200: "#EEF7F7",
        300: "#DEEEEE",
        400: "#BCDDDD",
        500: "#96BABB",
        600: "#70979A",
        700: "#4A7378",
        800: "#245056",
        900: "#15373A",
      },
      success: {
        50: "#EDF8F0",
        300: "#B2E1C2",
        400: "#8DD3A5",
        500: "#68C487",
        600: "#46B46B",
        700: "#388F55",
      },
      warning: {
        50: "#FFF5E5",
        300: "#FFD594",
        400: "#FFC161",
        500: "#FFAD2D",
        600: "#FA9800",
        700: "#C77900",
      },
      danger: {
        50: "#FFF3F0",
        300: "#FFBFAD",
        400: "#FF977A",
        500: "#FF7049",
        600: "#FF4714",
        700: "#E03000",
      },
      info: {
        50: "#F5F9FF",
        300: "#B3D7FF",
        400: "#80BCFF",
        500: "#4DA1FF",
        600: "#1A86FF",
        700: "#006CE6",
      },
    },
  },
  variants: {
    textColor: ["responsive", "hover", "focus", "disabled"],
    backgroundColor: [
      "responsive",
      "hover",
      "focus",
      "disabled",
      "active",
      "even",
    ],
    margin: ["responsive", "last"],
    padding: ["responsive", "last"],
    borderWidth: ["responsive", "last"],
    zIndex: ["responsive", "focus"],
    width: ["responsive", "hover"],
    overflow: ["responsive", "hover"],
    /* Custom */
    flexGap: ["responsive"],
    boxShadowColor: ["responsive", "hover", "focus"],
  },
  plugins: [flexGapPlugin, boxShadowColorPlugin],
  purge: [
    "public/index.html",
    "lib/es6_global/src/**/**.js",
    "node_modules/@colisweb/reason-toolkit/lib/es6_global/src/**/**.js",
  ].concat(
    process.env.STORYBOOK
      ? [
          "lib/es6_global/stories/**/**.js",
          "stories/**/**.js",
          ".storybook/**/**.js",
        ]
      : []
  ),
};
